<?php

/*admin*/
Route::Controller('admin/login','AdminLoginController');
Route::group(['before'=>'admin.auth','prefix'=>'admin'],function(){
    Route::Controller('index','AdminIndexController');
    Route::Controller('images','ImageController');
    Route::Controller('videos','VideoController');
    Route::Controller('news','AdminNewsController');
    Route::Controller('menus','AdminMenuController');
    Route::Controller('pages','AdminPageController');
    Route::Controller('download','DownloadController');
    Route::Controller('featured','FeaturedController');
    Route::Controller('social','SocialController');
});
Route::Controller('front','FrontController');

/* Front */
Route::Controller('/','FrontController');



