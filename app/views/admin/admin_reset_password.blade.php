<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Metronic | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"type="text/css"/>
    <link href="{{ URL::asset('admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"type="text/css"/>
    <link href="{{ URL::asset('admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"type="text/css"/>
    <link href="{{ URL::asset('admin/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet"type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ URL::asset('admin/assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('admin/assets/admin/pages/css/login3.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ URL::asset('admin/assets/global/css/components.css') }}" id="style_components" rel="stylesheet"type="text/css"/>
    <link href="{{ URL::asset('admin/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('admin/assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('admin/assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="{{ URL::asset('admin/assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<div id="baseUrl" data-baseurl="{{ url() }}"></div>
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="{{ URL::asset('admin/assets/admin/layout/img/logo-big.png') }}" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="admin_reset_password" action="{{ URL::to('admin/login/reset') }}" method="post">
        <h3 class="form-title">Reset Password</h3>

        @if(Session::has('failure'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
			<span>
			{{ Session::get('failure') }} </span>
        </div>
        @endif

        <div class="alert alert-danger danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>Please Enter Password.</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Password</label>

            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" id="password" placeholder="Password" name="password"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9"> Retype Password</label>

            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password"  placeholder="Retype Password" name="retype_password"/>

                <input name="id" type="hidden" value="{{ $verify['user_id'] }}"/>
                <input name="code" value="{{ $verify['code'] }}" type="hidden"/>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn green-haze pull-right">
                Reset <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>

        <div class="forget-password">

        </div>
        <br/>
        <br/>
    </form>
    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2015 &copy; Metronic.Dashboard.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('admin/assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ URL::asset('admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/uniform/jquery.uniform.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ URL::asset('admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"
        type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('admin/assets/global/plugins/select2/select2.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ URL::asset('admin/assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/js/login.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>