@extends('admin.layout.layout')
@section('title')
    {{ $detail->page_label }}
@stop

@section('breadcrumb')
    <li><a href="javascript:;">Featured Pages<i class="fa fa-angle-right"></i></a></li>
    <li><a href="javascript:;">{{ ucfirst($detail->page_label) }}</a></li>
@stop

@section('page_heading')
    {{ ucfirst($detail->page_label) }}
@stop

@section('content')
    <div class="row">
        <div class="col-md-9 article-block">
            <div class="row">
                <div class="col-md-9">Title: {{ ucfirst($detail->page_title) }}</div>
                <div class="col-md-3 text-right blog-tag-data-inner">
                    <ul class="list-inline">
                        <li>
                            <i class="fa fa-calendar"></i>
                            <a href="javascript:;">{{ date(date('d-m-Y H:i:s', strtotime($detail->created_at))) }} </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end news-tag-data-->
            <div class="text-justify">
                <a  class="fancybox" href="{{ URL::asset('web/admin/uploads/'.$detail->banner) }}">
                    <img src="{{ URL::to('image/web/admin/uploads/'.$detail->banner.'?s=800x320') }}" alt="">
                </a>
                <p>{{ $detail->page_content }}</p>
            </div>
            <hr>
        </div>
        <div class="col-md-3 blog-sidebar">
            <h3>Tags</h3>
            <ul class="list-inline sidebar-tags">
                @foreach($detail->tags as  $tag)
                <li><a href="javascript:;"><i class="fa fa-tags"></i> {{ $tag }} </a></li>
                @endforeach
            </ul>
            <div class="space20">
            </div>
        </div>
    </div>

    <div class="row">
    <div class="col-md-12">
        <a href="{{ URL::to('admin/featured/edit/'.$detail->id) }}" class="btn green btn-sm"><i class="fa fa-pencil-square-o"></i> Edit</a>
    </div>
    </div>
@stop


@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>
    <script>
        $('.fancybox').fancybox();
    </script>
    @stop