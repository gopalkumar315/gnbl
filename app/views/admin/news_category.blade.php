@extends('admin.layout.layout')

@section('title')
    Gallery | News
@stop

@section('breadcrumb')
    <li><a href="javascript:;">News </a></li>
@stop

@section('page_heading')
    News <small>Gallery</small>
    @stop

    @section('content')

    <!-- Datatable -->
    <div class="portlet box red-intense">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>News Categories
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">
                <a href="#addNews" role="button" class="btn btn-default btn-sm" data-toggle="modal"><i class="fa fa-pencil"></i> Add News Category </a>
                <a href="{{ URL::to('admin/news/addnews') }}" role="button" class="btn btn-default btn-sm" ><i class="fa fa-pencil"></i> Add News </a>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="news_category_table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Type</th>
                    <th class="hidden-xs">Total News</th>
                    <th>Icon</th>
                    <th class="hidden-xs">Created at</th>
                    <th class="hidden-xs">Updated at</th>
                    <th style="width:20%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- End Datatable -->

    <!-- Add news modal -->
    <div id="addNews" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header"><h4 class="modal-title"> Add News Category </h4></div>
                  <form action="#" method="post" class="form-horizontal" id="add_news_form">
                      <div class="modal-body">
                          <div class="form-body">
                              <div class="form-group">
                                  <label for="name" class="col-md-3 control-label">Name</label>
                                  <div class="col-md-8"><input type="text" name="name" class="form-control" id=""></div>
                              </div>
                              <div class="form-group">
                                  <label for="name" class="col-md-3 control-label">Icon</label>
                                  <div class="col-md-8"><input type="text" name="icon" class="form-control" id=""></div>
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
                          <button type="submit" class="btn green">Save</button>
                      </div>
                  </form>
              </div>
          </div>
    </div>

    <!-- Edit news modal -->
    <div id="editNews" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title"> Update News Category </h4></div>
                <form action="#" method="post" class="form-horizontal" id="updateForm">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name" class="col-md-3 control-label">Name</label>
                                <div class="col-md-8">
                                    <input type="text" name="name" class="form-control" id="">
                                    <input type="hidden" name="id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-3 control-label">Icon</label>
                                <div class="col-md-8"><input type="text" name="icon" class="form-control" id=""></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="{{ URL::asset('web/admin/js/admin_news_category.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            AdminNewsCategory.init();
        });
    </script>
@stop