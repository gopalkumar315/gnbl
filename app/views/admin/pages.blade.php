@extends('admin.layout.layout')

@section('stylesheets')

@stop

@section('title')
    Pages
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/pages') }}">CMS <i class="fa fa-angle-right"></i></a></li>
    <li><a href="javascript:;">Pages List</a></li>
@stop

@section('page_heading')
    Pages
@stop

@section('content')
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pin"></i>Pages
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">
                <a  role="button" data-toggle="modal" href="{{ URL::to('admin/pages/addpage') }}" class="btn btn-default btn-sm" >
                    <i class="fa fa-pencil"></i> Add Page</a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="albumId" data-value=""></div>

            <table class="table table-striped table-bordered table-hover" id="list_table">
                <thead>
                <tr>
                    <th style="width:10%">Id</th>
                    <th>Page Title</th>
                    <th>Status</th>
                    <th style="width:30%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $row)
                    <tr>
                        <td>{{ $row['id'] }}</td>
                        <td><a class="category_color" href="{{ URL::to('admin/pages/showpage/'.$row['id']) }}">{{ $row['page_label'] }}</a></td>
                        <td>
                            @if($row['status']==1)
                                <button class="btn btn-xs green" onclick="change_status(this)">Enabled</button>
                            @else
                                <button class="btn btn-xs red" onclick="change_status(this)">Disabled</button>
                            @endif
                        </td>
                        <td>
                            <button class="btn btn-xs btn-default" onclick="edit_page_show(this)"><i class="fa fa-edit"></i> Edit</button>
                            <button class="btn btn-xs btn-default" onclick="delete_page(this)"><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>

                    @foreach($row['level'] as $sub_cat)
                        <tr>
                            <td>{{ $sub_cat['id'] }}</td>
                            <td><a class="category_color" href="{{ URL::to('admin/pages/showpage/'.$sub_cat['id']) }}"> {{ $sub_cat['page_label'] }} </a> </td>
                            <td>
                                @if($sub_cat['status']==1)
                                    <button class="btn btn-xs green" onclick="change_status(this)">Enabled</button>
                                @else
                                    <button class="btn btn-xs red" onclick="change_status(this)">Disabled</button>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-xs btn-default" onclick="edit_page_show(this)"><i class="fa fa-edit"></i> Edit</button>
                                <button class="btn btn-xs btn-default" onclick="delete_page(this)"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                    @endforeach


                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('web/admin/js/admin_pages.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            AdminPages.init();
            initTable();
        });
    </script>
@stop