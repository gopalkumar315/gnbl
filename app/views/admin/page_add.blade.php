@extends('admin.layout.layout')

@section('stylesheets')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/select2/select2.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}">
    <link href="{{ URL::asset('web/admin/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>

@stop

@section('title')
    Add Page
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/pages') }}">CMS <i class="fa fa-angle-right"></i></a></li>
    <li><a href="javascript:;">Add Page</a></li>
@stop

@section('page_heading')
    Add  <small>Page</small>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-pin"></i>Add Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="#" method="post" id="add_form" class="form-horizontal" enctype="multipart/form-data" novalidate="novalidate">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3">Category</label>
                                <div class="col-md-4">
                                    <select class="form-control select2me select2-offscreen" name="parent_id" tabindex="-1" title="">
                                        <option value="">Select...</option>
                                        <option value="0">Main Menu</option>
                                        @foreach($categories as $row)
                                            <option value="{{ $row['id']  }}">{{ $row['page_label'] }}</option>
                                            @foreach($row['level'] as $sub_cat)
                                                <option value="{{ $sub_cat['id']  }}">{{ $sub_cat['page_label'] }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Label&nbsp;&nbsp;</label>
                                <div class="col-md-4">
                                    <input name="page_label" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Title&nbsp;&nbsp;</label>
                                <div class="col-md-4">
                                    <input name="page_title" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group last">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="page_content" name="page_content" rows="6" data-error-container="#editor2_error" ></textarea>
                                    <div id="editor2_error">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Banner</label>
                                <div class="col-md-4">
                                    <input type="file" name="file" class="form-control" id="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-4">
                                    <select class="form-control select2me select2-offscreen" name="status" tabindex="-1" title="">
                                        <option value="">Select...</option>
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disabled</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags" class="control-label col-md-3">Tags</label>
                                <div class="col-md-4">
                                    <input id="tags_1" placeholder="enter the tags" type="text" name="tags" class="form-control tags" style="display: none;">
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{ URL::to('admin/news/' ) }}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('web/admin/js/admin_pages.js') }}"></script>

    <script>
        CKEDITOR.replace( 'page_content', {
            //   filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            AdminPages.init();
            add_validation();
        });
    </script>
@stop