@extends('admin.layout.layout')

@section('title')
    Downloads
@stop

@section('breadcrumb')
    <li><a href="javascript:;">Downloads</a></li>
@stop

@section('page_heading')
    Download
    <small>List</small>
@stop

@section('content')
            <!-- Datatable -->
    <div class="portlet box red-intense">
        <div class="portlet-title">
            <div class="caption" style="text-transform: capitalize">
                <i class="fa fa-cloud-download"></i>Downloads List
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">
                <a  role="button" data-toggle="modal" href="#add_modal" class="btn btn-default btn-sm" >
                    <i class="fa fa-cloud-upload"></i> Upload</a>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="ajaxDatatable">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th>Download</th>
                    <th>Status</th>
                    <th class="hidden-xs">Created at</th>
                    <th style="width:20%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- End Datatable -->

    <!-- Add Modal -->
    <div id="add_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title"><i class="fa fa-cloud-upload"></i> Upload File</h4></div>
                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal" id="add_form">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name" class="col-md-3 control-label">Title</label>
                                <div class="col-md-8"><input type="text" name="title" class="form-control"></div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-3 control-label">Upload File</label>
                                <div class="col-md-8"><input type="file" name="file" class="form-control"></div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script src="{{ URL::asset('web/admin/js/download_list.js') }}"></script>
    <script src="{{ URL::asset('web/admin/js/zclip.js') }}"></script>

    <script>
        $(document).ready(function () {
            download_list.init();
        });
    </script>
    <script src="http://davidwalsh.name/demo/ZeroClipboard.js"></script>
@stop