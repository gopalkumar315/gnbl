@extends('admin.layout.layout')
@section('title')
    {{ $news_content->title }}
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/news/') }}">News</a><i class="fa fa-angle-right"></i></li>
    <li><a href="{{ URL::to('admin/news/newslist/'.$news_category->id) }}">{{ $news_category->name }}</a><i
                class="fa fa-angle-right"></i></li>
    <li><a href="#">{{ $news_content->title }}</a></li>
@stop

@section('page_heading')
    News
@stop

@section('content')
    <div class="row">
        <div class="col-md-9 article-block">
            <div class="row">
                <div class="col-md-9"><h4 style="margin-top:0px;">{{ $news_content->title }}</h4></div>
                <div class="col-md-3 text-right blog-tag-data-inner">
                    <ul class="list-inline">
                        <li>
                            <i class="fa fa-calendar"></i>
                            <a href="javascript:;">
                                {{ date('d-m-Y H:i:s', strtotime($news_content->created_at)) }} </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end news-tag-data-->
            <div class="text-justify">
                <p>{{ $news_content->description }}</p>
            </div>
            <hr>
        </div>
        <div class="col-md-3 blog-sidebar">
            <h3>News Tags</h3>
            <ul class="list-inline sidebar-tags">
                @foreach($tags as  $tag)
                    <li><a href="javascript:;"><i class="fa fa-tags"></i> {{ $tag }} </a></li>
                @endforeach
            </ul>
            <div class="space20">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ URL::to('admin/news/newsedit/'.$news_content->id) }}" class="btn green btn-sm"><i
                        class="fa fa-pencil-square-o"></i> Edit</a>
            <a href="{{ URL::to('admin/news/newslist/'.$news_category->id) }}" class="btn red btn-sm"><i
                        class="fa fa-reply"></i> Back</a>
        </div>
    </div>
@stop