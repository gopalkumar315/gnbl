@extends('admin.layout.layout')


@section('stylesheets')
    <link href="{{ URL::asset('admin/assets/global/plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .page-header.navbar.navbar-fixed-top{
            z-index:0;
        }
    </style>
@stop

@section('title')
    Gallery | Videos List
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/videos') }}">Albums <i class="fa fa-angle-right"></i> </a></li>
    <li><a href="javascript:;">{{ ucfirst($album->name) }}</a></li>
@stop

@section('page_heading')
    {{ ucfirst($album->name) }}
    <small>Album</small>
    @stop

    @section('content')
            <!-- Datatable -->
    <div class="portlet box red-intense">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-camera"></i>Videos
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">
                <a  role="button" data-toggle="modal" href="#addVideo" class="btn btn-default btn-sm" >
                    <i class="fa fa-pencil"></i> Add Video</a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="albumId" data-value="{{$album->id}}"></div>

            <table class="table table-striped table-bordered table-hover" id="video_list_table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Title</th>
                    <th class="hidden-xs">Video</th>
                    <th class="hidden-xs">Created at</th>
                    <th class="hidden-xs">Updated at</th>
                    <th style="width:20%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- End Datatable -->

    <!-- Add Videos -->
    <div id="addVideo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit_user" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Video</h4>
                </div>
                <form action="#" class="form-horizontal" method="post" id="add_video_form">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Title
                                </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" data-required="1" name="title" placeholder="Enter the title">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Video URL
                                </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" data-required="1" name="link" placeholder="Enter the video url">
                                </div>
                            </div>
                        </div>

                    </div>
                    <input name="album_id" value="{{$album->id}}" id="user_id" type="hidden"/>
                    <div class="modal-footer">
                        <button class="btn default" id="btn_close_add_model" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Album -->

    <!-- Edit Videos -->
    <div id="editVideo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit_user" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Video</h4>
                </div>
                <form action="#" class="form-horizontal" method="post" id="edit_video_form">
                    <div class="modal-body">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Video
                                </label>
                                <div class="col-md-9">
                                    <iframe width="100%" style="border:none;" height="315" id="iframe" >
                                    </iframe>
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Title
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" data-required="1" id="title" name="title" placeholder="Enter the title">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Video URL
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" data-required="1" id="link" name="link" placeholder="Enter the video url">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input name="video_id"  type="hidden"/>
                    <div class="modal-footer">
                        <button class="btn default" id="btn_close_add_model" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Video -->
@stop

@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ URL::asset('web/admin/js/admin_videos_list.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            AdminVideoList.init();
        });
    </script>
@stop