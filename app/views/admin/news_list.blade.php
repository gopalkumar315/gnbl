@extends('admin.layout.layout')

@section('title')
    News Category
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/news') }}">News <i class="fa fa-angle-right"></i> </a></li>
    <li><a href="javascript:;">{{ ucfirst($news->name) }}</a></li>
@stop

@section('page_heading')
    {{ ucfirst($news->name) }}
    <small>News List</small>
    @stop

    @section('content')
            <!-- Datatable -->
    <div class="portlet box red-intense">
        <div class="portlet-title">
            <div class="caption" style="text-transform: capitalize">
                <i class="fa fa-globe"></i>{{$news->name}}
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">
                <a  role="button" data-toggle="modal" href="{{URL::to('admin/news/addnews/')}}" class="btn btn-default btn-sm" >
                    <i class="fa fa-pencil"></i> Add News</a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="categoryId" data-value="{{$news->id}}"></div>

            <table class="table table-striped table-bordered table-hover" id="news_list_table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>News Title</th>
                    <th>Status</th>
                    <th class="hidden-xs">Created at</th>
                    <th class="hidden-xs">Updated at</th>
                    <th style="width:20%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- End Datatable -->
@stop


@section('scripts')
    <script src="{{ URL::asset('web/admin/js/admin_news_list.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            AdminNewsList.init();
        });
    </script>
@stop