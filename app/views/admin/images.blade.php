@extends('admin.layout.layout')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/dropzone/css/dropzone.css') }}"/>
<link href="{{ URL::asset('web/admin/assets/global/plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css"/>

<style>
    .dropzone {
        min-height: 226px;
    }
    .page-header.navbar.navbar-fixed-top{
        z-index:0;
    }

</style>
@stop

@section('title')
Gallery | Images
@stop

@section('breadcrumb')
<li><a href="{{ URL::to('admin/images') }}">Albums <i class="fa fa-angle-right"></i> </a></li>
<li><a href="#">{{ ucfirst($album->name) }}</a></li>
@stop

@section('page_heading')
    {{ ucfirst($album->name) }}
{{--<small> Album</small>--}}
@stop

@section('content')
<!-- Datatable -->
<div class="portlet box red-intense">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Images
        </div>

        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>

        <div class="actions">

            <a  role="button" data-toggle="modal" href="#responsive" class="btn btn-default btn-sm" >
                <i class="fa fa-upload"></i> Upload Image </a>
        </div>
    </div>
    <div class="portlet-body">
        <div id="albumId" data-value="{{$album->id}}"></div>

        <table class="table table-striped table-bordered table-hover" id="image_list_table">
            <thead>
            <tr>
                <th>id</th>
                <th>Title</th>
                <th class="hidden-xs">Image</th>
                <th class="hidden-xs">Created at</th>
                <th class="hidden-xs">Updated at</th>
                <th style="width:20%" class="hidden-xs">Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<!-- End Datatable -->

<!-- Modal responsive -->
<div id="responsive" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ URL::to('admin/images/upload') }}" id="my-dropzone"
                              class="dropzone dz-clickable">
                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                            <input type="hidden" name="id" value="{{$album->id}}">
                        </form>

                        <div class="form-group">
                            <br>
                            <input type="text" placeholder="enter the title" name="title" id="title"
                                   class="form-control">
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="btn default">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal responsive -->
<div id="update_image" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Image</h4>
            </div>
            <form action="{{ URL::to('admin/image/update') }}" method="post" id="updateForm"
                  enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <img src="" alt="" class="img-responsive img-thumbnail" width="200px" height="200px"
                                     id="imageUpdate">
                            </div>
                            <div class="form-group">
                                <input type="file" name="file" class="form-control" id="previewImage">
                            </div>

                            <div class="form-group">
                                <input type="text" name="title" placeholder="enter the title" class="form-control"
                                       id="updateTitle">
                            </div>
                            <input type="hidden" name="id" class="form-control" id="updateId">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="btn default">Close</button>
                    <button type="submit" class="btn green default">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>


@stop

@section('scripts')
{{--<script src="{{ URL::asset('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>--}}
{{--<script src="{{ URL::asset('admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>--}}
<script src="{{ URL::asset('web/admin/assets/global/plugins/dropzone/dropzone.js') }}"></script>
<script src="{{ URL::asset('web/admin/js/admin_images_list.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js') }}"></script>


<script>
    jQuery(document).ready(function () {
        AdminImagesList.init();
    });


</script>
@stop