<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="@if(Request::segment(2)  == 'index') active @endif">
                <a href="{{ URL::to('admin/index')  }}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="@if(in_array(Request::segment(2),array('videos','images'))) active @endif">
                <a href="javascript:;">
                    <i class="fa fa-picture-o"></i>
                    <span class="title">Gallery</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">

                    <li class="@if(Request::segment(2)=='images')) active @endif">
                        <a href="{{ URL::to('admin/images') }}"><i class="fa fa-file-image-o"></i> Images</a>
                    </li>
                    <li class="@if(Request::segment(2)=='videos')) active @endif">
                        <a href="{{ URL::to('admin/videos') }}"><i class="fa fa-youtube-play"></i> Videos</a>
                    </li>
                </ul>
            </li>
            <li class="<li @if(Request::segment(2)  == 'news') active @endif">
                <a href="{{ URL::to('admin/news') }}">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">News</span>
                </a>
            </li>

            <li class="@if(Request::segment(2)=='pages')) active @endif">
                <a href="javascript:;">
                    <i class="icon-pin"></i>
                    <span class="title">CMS</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="@if(Request::segment(2)=='pages')) active @endif">
                        <a href="{{ URL::to('admin/pages') }}"><i class="fa fa-file"></i> Pages</a>
                    </li>
                </ul>
            </li>
            <li class="@if(Request::segment(2)  == 'featured') active @endif" >
                <a href="javascript:;">
                    <i class="fa fa-magic"></i>
                    <span class="title">Featured Pages</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{ URL::to('admin/featured/page/1') }}">Principal Message</a></li>
                    <li><a href="{{ URL::to('admin/featured/page/2') }}">Admission</a></li>
                    <li><a href="{{ URL::to('admin/featured/page/3') }}">Price</a></li>
                    <li><a href="{{ URL::to('admin/featured/page/4') }}">Events</a></li>
                    <li><a href="{{ URL::to('admin/featured/page/5') }}">Teachers</a></li>
                </ul>
            </li>
            <li class="<li @if(Request::segment(2)  == 'social') active @endif">
                <a href="{{ URL::to('admin/social') }}">
                    <i class="fa fa-link"></i>
                    <span class="title">Social Links</span>
                </a>
            </li>
            <li class="<li @if(Request::segment(2)  == 'download') active @endif">
                <a href="{{ URL::to('admin/download') }}">
                    <i class="fa fa-cloud-download"></i>
                    <span class="title">Downloads</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->