<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner text-center">
        2015 &copy; Web Mistry A Development Studios.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('web/admin/assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- DataTables -->
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- End DataTables -->

<!-- Toaster -->
<script src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-toastr/toastr.min.js') }}"></script>
<script src="{{ URL::asset('web/admin/assets/admin/pages/scripts/ui-toastr.js') }}"></script>
<!-- Toaster -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ URL::asset('web/admin/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/admin/pages/scripts/index.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('web/admin/assets/admin/pages/scripts/tasks.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- Date picker component picker -->
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ URL::asset('web/admin/assets/admin/pages/scripts/components-pickers.js') }}"></script>
<script src="{{ URL::asset('web/admin/assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

<!-- End Date picker component picker -->

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        UIToastr.init(); //init toaster
        ComponentsPickers.init(); // init datepicker
    });

    show_loader=function(){
        Metronic.blockUI({animate: true});
    };

    hide_loader=function() {
        Metronic.unblockUI();
    };
</script>