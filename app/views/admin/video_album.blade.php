@extends('admin.layout.layout')

@section('title')
    Gallery | Video | Album
@stop

@section('breadcrumb')
    <li><a href="javascript:;">Gallery</a><i class="fa fa-angle-right"></i></li>
    <li><a href="javascript:;">Video Albums</a></li>
@stop

@section('page_heading')
    Video Albums
    <small>List</small>
    @stop

    @section('content')
            <!-- BEGIN DASHBOARD STATS -->

    <!-- Datatable -->
    <div class="portlet box red-intense">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-camera"></i>Albums
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

            <div class="actions">

                <a href="#addAlbum" role="button" class="btn btn-default btn-sm" data-toggle="modal">
                    <i class="fa fa-pencil"></i> Add Album </a>

            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="album_table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th class="hidden-xs">Total Videos</th>
                    <th class="hidden-xs">Created at</th>
                    <th class="hidden-xs">Updated at</th>
                    <th style="width:20%" class="hidden-xs">Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- End Datatable -->

    <!-- Add Album -->
    <div id="addAlbum" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit_user" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Album</h4>
                </div>
                <form action="#" class="form-horizontal" method="post" id="add_album_form">
                    <div class="modal-body">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name
                                </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" data-required="1" name="name"
                                           placeholder="Album name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input name="user_id" id="user_id" type="hidden"/>

                    <div class="modal-footer">
                        <button class="btn default" id="btn_close_add_model" data-dismiss="modal" aria-hidden="true">
                            Close
                        </button>
                        <button type="submit" class="btn green">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Album -->

    <!-- Edit Album -->
    <div id="editAlbum" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit_user" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Album</h4>
                </div>
                <form action="#" class="form-horizontal" method="post" id="edit_album_form">
                    <div class="modal-body">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name
                                </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" data-required="1" name="name"
                                           placeholder="Album name">
                                    <input type="hidden" name="id" value=""/>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn default" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button type="submit" class="btn green">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Model -->
@stop

@section('scripts')
    <script src="{{ URL::asset('web/admin/js/admin_video_album.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            AdminVideosAlbum.init();
        });
    </script>
@stop