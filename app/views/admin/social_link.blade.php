@extends('admin.layout.layout')
@section('title')
    Social Links
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/social') }}">Social Links </a></li>
@stop

@section('page_heading')
    Social Links
@stop

@section('content')
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-link"></i>Social Links
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>

        </div>
        <div class="portlet-body form">
            <form action="#" method="post" id="update_form" class="form-horizontal" enctype="multipart/form-data" novalidate="novalidate">
                <div class="form-body">

                    @foreach($social_list as $row)
                        <div class="form-group">
                            <label class="control-label col-md-3">{{ $row->name }} &nbsp;&nbsp;</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-icon">
                                        <i class="fa {{ $row->icon }} fa-fw"></i>
                                        <input id="newpassword"  value="{{ $row->link }}" class="form-control text" type="url" name="{{ $row->name }}" placeholder="">
                                    </div>
                                    <span class="input-group-btn">
                                        <a href="{{ $row->link }}" target="_blank" id="genpassword" class="btn red" type="button">Visit <i class="fa fa-arrow-right fa-fw"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">Update</button>
                            <a href="{{ URL::to('admin/index' ) }}" class="btn default">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('web/admin/js/social_links.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            SocialLink.init();
        });
    </script>
@stop