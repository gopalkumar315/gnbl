@extends('admin.layout.layout')

@section('stylesheets')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/select2/select2.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}">
    <link href="{{ URL::asset('web/admin/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>

@stop

@section('title')
    Edit News
@stop

@section('breadcrumb')
    <li><a href="{{ URL::to('admin/news') }}">News <i class="fa fa-angle-right"></i></a></li>
    <li><a href="javascript:;"  style="text-transform: capitalize" id="new_title"> </a></li>
@stop

@section('page_heading')
    Edit  <small>News</small>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Edit News
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="#" method="post" id="edit_news_form" class="form-horizontal" novalidate="novalidate">
                        <div id="newsId" data-value="{{ $news_id }}"></div>
                        <input type="hidden" name="id" value="{{ $news_id }}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title&nbsp;&nbsp;</label>
                                <div class="col-md-4">
                                    <input name="title" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">News Category</label>
                                <div class="col-md-4">
                                    <select class="form-control select2me select2-offscreen" name="news_category_id" tabindex="-1" title="">
                                        <option value="">Select...</option>
                                        @foreach($news_category as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group last">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="description" name="description" rows="6" data-error-container="#editor2_error" ></textarea>
                                    <div id="editor2_error">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-4">
                                    <select class="form-control select2me select2-offscreen" name="status" tabindex="-1" title="">
                                        <option value="">Select...</option>
                                        <option value="1">Enable</option>
                                        <option value="0">Disabled</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags" class="control-label col-md-3">Tags</label>
                                <div class="col-md-4">
                                    <input id="tags_1" placeholder="enter the tags" type="text" name="tags" class="form-control tags" style="display: none;">
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Update</button>
                                    <a href="{{ URL::to('admin/news/' ) }}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('web/admin/js/admin_news_edit.js') }}"></script>



    <script>
        jQuery(document).ready(function () {
            AdminEditNews.init();
        });
    </script>
@stop