@extends('admin.layout.layout')

@section('stylesheets')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/select2/select2.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('web/admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}">
    <link href="{{ URL::asset('web/admin/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>

@stop

@section('title')
    Update Page
@stop

@section('breadcrumb')
    <li><a href="javascript:;">Featured Pages <i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ URL::to('admin/featured/page/'.$detail->id) }}" >{{ $detail->page_label }} </a></li>
@stop

@section('page_heading')
    {{ $detail->page_label }}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-pin"></i>Update
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title="">                        </a>

                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="#" method="post" id="edit_form" class="form-horizontal" enctype="multipart/form-data" novalidate="novalidate">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3">Label&nbsp;&nbsp;</label>
                                <div class="col-md-4">
                                    <input name="page_label" readonly="readonly" type="text" class="form-control">
                                    <input type="hidden" name="id" value="{{ $detail->id }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Title&nbsp;&nbsp;</label>
                                <div class="col-md-4">
                                    <input name="page_title" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group last">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="page_content" name="page_content" rows="6" data-error-container="#editor2_error" ></textarea>
                                    <div id="editor2_error">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Banner</label>
                                <div class="col-md-4">
                                    <img src="" id="banner_img"  width="200px" height="200px" class="img-responsive img-thumbnail" alt="">
                                    <br><br>
                                    <input type="file" name="file" onchange="readURL(this)" class="form-control" id="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags" class="control-label col-md-3">Tags</label>
                                <div class="col-md-4">
                                    <input id="tags_1" placeholder="enter the tags" type="text" name="tags" class="form-control tags" style="display: none;">
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{ URL::to('admin/news/' ) }}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script src="{{URL::asset('web/admin/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL::asset('web/admin/assets/global/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('web/admin/js/featured_pages.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            FeaturedPages.init();
        });
    </script>



@stop