<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Reset Password</title>
</head>
<body>

    <h3>Hello {{ $username }},</h3>
    <p>
        <a href="{{ URL::to('mylove007/login/verify/'.$confirmation_code.'/'.$user_id) }}">Click here to reset your password</a>
    </p>
</body>
</html>