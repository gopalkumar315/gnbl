@extends('front.layout.layout')
@section('title')
    Video Album | {{ ucfirst($data['album']->name) }}
@stop

@section('content')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-md-3">
            <div class="sidebar">
                <h1 class="sidebar-heading text-center">Quick links</h1>
                <ul class="nav nav-pills nav-stacked">
                    @foreach($data['albums'] as $row)
                        <li><a href="{{ URL::to('videolist/'.$row->id.'/'.$row->name) }}"><i class="fa fa-long-arrow-right"></i> {{ ucfirst($row->name).' ('. $row->videos()->count() .')' }} </a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-9">

            <div class="row">
                <div class="col-md-12">
                    <div class="upper-links">
                        <ul class="list-inline">
                            <li><a href="{{ URL::to('/') }}">Home<i class="fa fa-caret-right"></i></a></li>
                            <li><a href="{{ URL::to('albums') }}">Albums<i class="fa fa-caret-right"></i></a></li>
                            <li><a href="javascript:;">{{ ucfirst($data['album']->name) }}<i class="fa fa-caret-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="heading">{{ ucfirst($data['album']->name) }} </h1> <br>
                    <div class="inner-content">
                        <div class="row">
                            @foreach($data['images'] as $row)
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="gallery video-album">
                                        <img class="img-responsive img-thumbnail" src="http://img.youtube.com/vi/{{ $row->link }}/mqdefault.jpg">
                                        <a class="overlay fancybox" title="{{ $row->title }}" href="https://www.youtube.com/watch?v={{ $row->link }}" rel="gallery-group">
                                            <i class="fa fa-play-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@stop

@section('scripts')
  <script type="text/javascript">
  jQuery(document).ready(function() {
      $(".fancybox").click(function() {
        $.fancybox({
          'padding'		: 0,
          'autoScale'		: false,
          'transitionIn'	: 'none',
          'transitionOut'	: 'none',
          'title'			: this.title,
          'width'			: 640,
          'height'		: 385,
          'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
          'type'			: 'swf',
          'swf'			: {
          'wmode'				: 'transparent',
          'allowfullscreen'	: 'true'
          }
        });

        return false;
      });
});
  </script>
@stop
