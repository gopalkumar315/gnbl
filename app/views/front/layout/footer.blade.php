<div class="footer-bg">
    <div class="container">
        <div class="row">
            <h2 class="footer-h2">Quick Links</h2>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="footer">
                    <ul id="footerMenus">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="footer">
                    <ul id="footerNews"></ul>
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="footer">
                    <ul>
                        <li><a href="{{ URL::to('otherpage/1/Principal Message') }}">Principal Message</a></li>
                        <li><a href="{{ URL::to('otherpage/2/admission') }}">Admissions</a></li>
                        <li><a href="{{ URL::to('otherpage/3/price') }}">Prices</a></li>
                        <li><a href="{{ URL::to('otherpage/4/events') }}">Events</a></li>
                        <li><a href="{{ URL::to('otherpage/5/teachers') }}">Teachers</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="footer">
                    <p>GNBL |  Satnampura |<br>
                        Phagwara, Punjab, India </p>
                    <p>
                        Telephone: +91-1824-60608 <br>
                        E-mail: info@gnbl.in
                    </p>
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="footer">
                    <ul class="list-inline" id="social_links"></ul>
                </div>
            </div>


        </div>
    </div>

    <div class="copyright text-center">
        <p>Copyright 2015  All Rights Gnbl.in  </p>
    </div>
</div>
<script type="text/javascript" src="{{ URL::asset('web/front/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/front/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/front/js/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('web/front/js/jquery.mousewheel-3.0.6.pack.js') }}"></script>
{{--<script src="https://cameronspear.com/downloads/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>--}}
<script type="text/javascript" src="{{ URL::asset('web/front/js/global.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
        global.init();
    });

</script>
