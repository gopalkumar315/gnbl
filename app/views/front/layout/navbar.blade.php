<div class="bg-top">
    <div class="container master">
        <div class="row">
            <div class="col-md-1">
                <div class="logo">
                    <a href="{{ URL::to('/') }}"><img src="{{ URL::asset('web/front/img/logo.png') }}" width="70px"></a>
                </div>
            </div>
            <div class="col-md-5">
                <h3 class="name">Guru Nanak Bhai Lalo College for Women, Phagwara</h3>
            </div>
            <div class="col-md-4">
                <input type="text" class="search-bar" placeholder="Search">
            </div>
            <div class="col-md-2">
                <div class="strip-details ">
                <span>
                  +91 - 1824-60608<br>
                  info@gnbl.in
                </span>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav" id="nav_menu">
            <li><a href="{{ URL::to('/') }}">HOME</a></li>
        </ul>
    </div><!--/.nav-collapse -->
</nav>