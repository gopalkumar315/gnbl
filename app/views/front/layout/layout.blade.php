<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        GNBL | @section('title') Guru Nanak Bhai Lalo College for Women @stop
        @yield('title')
    </title>
    <meta name="keywords" content="gnbl.in, gnbl,GNBL Ramgarhia College for Women,Phagwara,Punjab,Mrs. Kusum Chauhan, Res 0091-1824-265298, Dr. (Mrs.) Rupinderjeet Kaur Bains, Kapurthala, Punjab, latest news, latest album, Upcoming events, @yield('keywords')"/>
    <meta name="description" content="GNBL Ramgarhia College for Women is situated in Phagwara of Punjab state (Province) in India. GNBL Ramgarhia College for Women, Phagwara Punjab is a recognised institute / college. "/>
    @include('front.layout.header')
    @yield('stylesheets')
</head>
<body>
<div id="baseUrl" data-baseurl="{{ url() }}"></div>
@include('front.layout.navbar')

@yield('content')

@include('front.layout.footer')
@yield('scripts')
</body>
</html>