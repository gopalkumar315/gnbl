@extends('front.layout.layout')
@section('title') Albums @stop

@section('content')
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <h1 class="sidebar-heading text-center">Quick links</h1>
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($data['albums'] as $row)
                            <li><a href="{{ URL::to('albumimages/'.$row->id.'/'.$row->name) }}"><i class="fa fa-long-arrow-right"></i> {{ ucfirst($row->name).' ('. $row->photos()->count() .')' }} </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-12">
                        <div class="upper-links">
                            <ul class="list-inline">
                                <li><a href="{{ URL::to('/') }}">Home<i class="fa fa-caret-right"></i></a></li>
                                <li><a href="{{ URL::to('albums') }}">Albums<i class="fa fa-caret-right"></i></a></li>
                                <li><a href="javascript:;">{{ ucfirst($data['album']->name) }}<i class="fa fa-caret-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="heading">{{ ucfirst($data['album']->name) }} </h1>
                        <div class="inner-content">
                            <div class="row">
                                @foreach($data['images'] as $row)
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="gallery">
                                            <img class="img-responsive img-thumbnail" src="{{ URL::to('image/web/admin/uploads/'.$row->file.'?s=307x285') }}">
                                            <a class="overlay fancybox" title="{{ $row->title }}" href="{{ URL::asset('web/admin/uploads/'.$row->file) }}" rel="gallery-group">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>


@stop
