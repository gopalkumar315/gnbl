@extends('front.layout.layout')
@section('title')
    {{ $content->page_label }}
@stop

@section('keywords')
    {{ $content->tags }}
@stop

@section('content')
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <h1 class="sidebar-heading text-center">Quick links</h1>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a @if($data['root']['id'] == $content->id) class="sidebar_active"@endif href="{{ URL::to('/page/'.$data['root']['id'].'/'.$data['root']['page_label']) }}"><i class="fa fa-long-arrow-right"></i> {{ ucfirst($data['root']['page_label']) }} </a> </li>
                        @foreach($data['parent_pages'] as $row)
                            <li>
                                <a @if($row->id == $content->id) class="sidebar_active"@endif href="{{ URL::to('/page/'.$row->id.'/'.$row->page_label) }}"><i class="fa fa-long-arrow-right"></i> {{ ucfirst($row->page_label) }} </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="upper-links">
                            <ul class="list-inline">

                                @foreach($data['breadcrumb'] as $row)
                                    @if($row['id'] != $content->id )
                                        <li><a href="{{ URL::to('/page/'.$row['id'].'/'.$row['page_label']) }}"> {{ ucfirst($row['page_label']) }} <i class="fa fa-caret-right"></i> </a></li>
                                    @endif
                                @endforeach
                                <li><a href="{{ URL::to('/page/'.$content->id.'/'.$content->page_label) }}"> {{ ucfirst($content->page_label) }} <i class="fa fa-caret-right"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ URL::to('image/web/admin/uploads/'.$content->banner.'?s=1000x320') }}" class="img-responsive" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="inner-content">
                            <h2>{{ ucfirst($content->page_title) }}</h2>
                            <div>{{ $content->page_content }}</div>


                            @if($content->parent_id != 0 )
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(count($data['sub_pages'])>0)
                                            <h4>Other links</h4>
                                        @endif

                                        <ul class="list-inline">
                                            @foreach($data['sub_pages'] as $row)
                                                <li><a href="{{ URL::to('/page/'.$row->id.'/'.$row->page_label) }}"> {{ ucfirst($row->page_label) }} </a> </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <br><br><br><br>
    </div>
@stop
