@extends('front.layout.layout')
@section('title')
    Video Album
@stop

@section('content')
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <h1 class="sidebar-heading text-center">Quick links</h1>
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($data['albums'] as $row)
                            <li><a href="{{ URL::to('videolist/'.$row->id.'/'.$row->name) }}"><i class="fa fa-long-arrow-right"></i> {{ ucfirst($row->name).' ('. $row->videos()->count() .')' }} </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-12">
                        <div class="upper-links">
                            <ul class="list-inline">
                                <li><a href="{{ URL::to('/') }}">Home<i class="fa fa-caret-right"></i></a></li>
                                <li><a href="{{ URL::to('albums') }}">Video Albums<i class="fa fa-caret-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="heading">Video Albums</h1>
                        <br>
                        <div class="inner-content">
                            <div class="row">
                                @foreach($data['albums'] as $row)
                                    @if(count($row->videos()->first()) >  0)
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="gallery video-album">
                                                <img class="img-responsive img-thumbnail" src="http://img.youtube.com/vi/{{ $row->videos()->first()->link }}/mqdefault.jpg">
                                                <a class="overlay" href="{{ URL::to('videolist/'.$row->id.'/'.$row->name) }}" rel="gallery-group">
                                                    <span class="video-title"> {{ ucfirst($row->name) }} </span>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
@stop
