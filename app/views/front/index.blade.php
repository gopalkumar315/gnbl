@extends('front.layout.layout')
@section('title')
    Guru Nanak Bhai Lalo College for Women
@stop

@section('content')
        <!-- Banner -->
    <div class="banner">
        <img src="{{ URL::asset('web/front/img/banner.png') }}" class="img-responsive">
    </div>
    <!-- Banner End -->

    <div class="container">
        <div class="row text-center hidden-sm hidden-xs board">
            <div class="col-md-3 sky_board"> <a href="{{ URL::to('otherpage/2/admission') }}">ADMISSIONS</a>  </div>
            <div class="col-md-3 orange_board"><a href="{{ URL::to('otherpage/3/price') }}"> PRICES </a> </div>
            <div class="col-md-3 red_board"><a href="{{ URL::to('otherpage/4/events') }}"> EVENTS</a> </div>
            <div class="col-md-3 green_board"><a href="{{ URL::to('otherpage/5/teachers') }}"> TEACHERS </a> </div>
        </div>

        <div class="row board">
            <div class="col-md-6 no-pad news_type_list">
                <h3 class="news_heading">News categories</h3>
                @foreach($data['news'] as $row)
                    <a href="javascript:;" onclick="get_news_list(this)" id="{{ $row->id }}" class="news_type news_link"> <i class=" {{ $row->icon }}"></i> {{ $row->name }}</a>
                @endforeach

            </div>

            <div class="col-md-6 news-master">
                <h3 class="news_heading ajax_news_heading">News</h3>
                <div class="row">
                    <div class="col-md-12 news_title_link ajax_news_list">

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="msg_bg">
        <div class="container">
            <div class="col-md-6 anchor_man_img">
                <img src="{{ URL::asset('web/front/img/anchor_man.png') }}" class="text-center img-responsive">
            </div>

            <div class="col-md-6">
                <h3 class="msg_title">{{ $data['principal_message']->page_label }}</h3>
                <div class="msg"> {{ Str::limit($data['principal_message']->page_content,500) }} </div>
                <div class="msg-btn-master">
                    <a href="{{ URL::to('/otherpage/'.$data['principal_message']->id.'/'.$data['principal_message']->page_label) }}" class="msg-btn">Read more</a>
                </div>
            </div>

        </div>
    </div>


    <div class="gallery-master">
        <div class="container">
            <div class="row-fluid">
                <h2 class="gallery-h2">Gallery</h2>
                @foreach($data['albums'] as $row)
                 @if(count($row->photos()->first()) >  0)
                    <div class="col-md-2 col-sm-3 col-xs-6">
                        <div class="gallery">
                            <img class="img-responsive" src="{{ URL::to('image/web/admin/uploads/'.$row->photos()->first()->file.'?s=285x285') }}">
                            <a class="overlay" href="{{ URL::to('albumimages/'.$row->id.'/'.$row->name) }}" rel="gallery-group">
                                <span class="album_name">{{ ucfirst($row->name) }}</span>
                            </a>
                        </div>
                    </div>
                 @endif
                @endforeach
            </div>

        </div>
    </div>

    <div class="rectr-bg">
        <div class="container">
            <h2 class="rctr-h2">Recruiter</h2>
            <div class="row">
                <div class="col-md-2 col-sm-2 col-sm-offset-1 col-md-offset-1">
                    <img class="img-responsive" src="{{ URL::asset('web/front/img/rctr1.jpg') }}">
                </div>
                <div class="col-md-2 col-sm-2">
                    <img class="img-responsive" src="{{ URL::asset('web/front/img/rctr2.jpg') }}">
                </div>
                <div class="col-md-2 col-sm-2">
                    <img class="img-responsive" src="{{ URL::asset('web/front/img/rctr3.jpg') }}">
                </div>
                <div class="col-md-2 col-sm-2">
                    <img class="img-responsive" src="{{ URL::asset('web/front/img/rctr4.jpg') }}">
                </div>
                <div class="col-md-2 col-sm-2">
                    <img class="img-responsive" src="{{ URL::asset('web/front/img/rctr5.jpg') }}">
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts')
    <script>
        $(document).ready(function(){
            //define in global
            get_news_list(0);
        })
    </script>
@stop