@extends('front.layout.layout')
@section('title')
    Contact Us
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="heading">Contact us</h1>
                <div class="map">
                    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:300px;width:100%;"><div id="gmap_canvas" style="height:300px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.themecircle.net" id="get-map-data">themecircle.net</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:16,center:new google.maps.LatLng(31.22401979999999,75.77080130000002),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(31.22401979999999, 75.77080130000002)});infowindow = new google.maps.InfoWindow({content:"<b>Guru Nanak Bhai Lalo College for Women,</b><br/>Satnampura, <br/>Phagwara, Punjab " });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                </div>
            </div>
        </div>
    </div>

    <div class="container master">
        <div class="row">
            <div class="col-md-6">
                <div class="cu">
                    <input type="text" class="cu-control" placeholder="Name">
                    <input type="text" class="cu-control" placeholder="Email">
                    <textarea rows="4" class="cu-area" placeholder="Message"></textarea>
                    <a href="" class="cu-btn">Send</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pin text-center">
                    <i class="fa fa-map-marker"></i>
                    <p class="address">GNBL Ramgarhia College for Women,<br>
                        Satnampura,<br>
                        Phagwara, Punjab, India<br>
                        Telephone: +91-1824-60608, 228508, 501920<br>
                        E-mail: gnblramgarhia@yahoo.com</p>
                </div>
            </div>
        </div>
    </div>


@stop
