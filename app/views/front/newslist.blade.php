@extends('front.layout.layout')
@section('title')
 News | {{ $news['news_category']->name }}
@stop

@section('content')
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <h1 class="sidebar-heading text-center"> <i class="{{ $news['news_category']->icon }}"></i> {{ $news['news_category']->name }} </h1>
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($news['news_sidebar'] as $row)
                            <li><a href="{{ URL::to('front/newslist/'.$news['news_category']->id.'/'.strtolower($row->month).'/'.strtolower($row->year)) }}"><i class="fa fa-long-arrow-right"></i>  {{ $row->month.' '.$row->year.' ('.$row->total.')' }} </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-12">
                        <div class="upper-links">
                            <ul class="list-inline">
                                <li><a href="{{ URL::to('/') }}"> <i class="fa fa-home"> </i> Home </a></li>
                                <li><a href="javascript:;"> <i class="{{ $news['news_category']->icon }}"></i> {{ $news['news_category']->name }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h1 class="heading">News</h1>
                        <div class="inner-content">
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($news['list'] as $row)
                                        <div class="news">
                                            <h4 class="no_bottom_m">{{ $row->title }}</h4>
                                            <div class="date_time"><i>posted on:- </i>({{ date('d-M-Y h:i:s', strtotime($row->created_at)) }}) </div>
                                            <p> {{ Str::limit(strip_tags($row->description),100) }}</p>
                                            <a href="{{ URL::to('front/shownews/'.$row->id.'/'.$row->title) }}" class="readmore-btn">Read more</a>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

@stop
