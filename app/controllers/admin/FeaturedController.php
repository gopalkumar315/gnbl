<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 10/1/2015
 * Time: 2:57 PM
 */
class FeaturedController extends BaseController
{
    public function getPage($id)
    {
        $page_content=Featured::find($id);
        $page_content->tags = explode(',', $page_content->tags);
        return View::make('admin/featured_page')->with('detail',$page_content);
    }

    public function getEdit($id)
    {
        $page_content=Featured::find($id);
        return View::make('admin/featured_edit')->with('detail',$page_content);
    }

    public function postEdit()
    {
        $id = Input::get('id');
        $page_detail = Featured::find($id);
        $page_detail->response_status='success';
        return Response::json($page_detail, 200);
    }

    public function postUpdate()
    {
        $input = Input::all();
        $id = $input['id'];

        if(Input::hasFile('file')){
            $destinationPath = base_path().'\web\admin\uploads';
            $file = Input::file('file');
            $filename = trim(str_random(10).$file->getClientOriginalName());
            $file->move($destinationPath, $filename);
            $input['banner'] = $filename;
        } else {
            unset($input['banner']);
        }
        unset($input['id']);
        unset($input['file']);
        unset($input['/']);

        Featured::where('id', $id)->update($input);
        return Response::json(array('status' => 'success'), 200);
    }
}