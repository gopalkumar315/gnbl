<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/22/2015
 * Time: 7:59 PM
 */
class AdminNewsController extends BaseController
{
    public function getIndex()
    {
        return View::make('admin/news_category');
    }

    public function postNewscategorylist()
    {
        $albums = NewsCategory::select(array('news_category.id','news_category.name',DB::raw('count(news.news_category_id) as total'),'news_category.icon',DB::raw('DATE_FORMAT(news_category.created_at,"%d-%b-%Y %H:%i:%s") as created'  ),DB::raw('DATE_FORMAT(news_category.updated_at,"%d-%b-%Y %H:%i:%s") as updated')))
            ->join('news', 'news_category.id', '=', 'news.news_category_id','left outer')->groupBy('news_category.id');

        return Datatables::of($albums)
            ->add_column('action', '<button class="btn btn-default btn-xs" onclick="newsEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-default btn-xs" onclick="newsDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')
            ->make();
    }

    public function postAddnewscategory()
    {
        NewsCategory::Create(array(
           'name'=>Input::get('name'),
           'icon'=>Input::get('icon')
        ));

        return Response::json(array('status' => 'success'), 200);
    }

    public function postDeletecategory()
    {
        $id = Input::get('id');
        NewsCategory::destroy($id);
        return Response::json(array('status' => 'success'), 200);
    }

    /* Edit news categroy*/
    public function postEditnewscategory()
    {
        $id = Input::get('id');
        $news=NewsCategory::find($id);
        return Response::json($news, 200);
    }

    /* Update news categroy*/
    public function postUpdatenewscategory()
    {
        $id = Input::get('id');
        $name = Input::get('name');
        $icon = Input::get('icon');
        NewsCategory::where('id',$id)->update(array(
            'name' => $name,
            'icon'=>$icon
        ));

        return Response::json(array('status' => 'success'), 200);
    }


    public function getNewslist($id)
    {
        $news = NewsCategory::find($id);
        return View::make('admin/news_list')->with('news', $news);
    }

    public function getAddnews()
    {
        $news_category = NewsCategory::all();
        return View::make('admin/news_add')->with('news_category', $news_category);
    }

    public function postInsertnews()
    {
        News::create(array(
           'title'=>Input::get('title'),
            'news_category_id' => Input::get('news_category_id'),
            'description' => Input::get('description'),
            'status' => Input::get('status'),
            'tags'=>Input::get('tags')
        ));
    }

    /*News List*/
    public function postNewslist($id)
    {
        $news_content=News::select(array('id','title','status',DB::raw('DATE_FORMAT(created_at,"%d-%b-%y %H:%i:%s") as created'),DB::raw('DATE_FORMAT(updated_at,"%d-%b-%y %H:%i:%s") as updated')))->where('news_category_id',$id);
        return Datatables::of($news_content)
            ->edit_column('status',function($result){
                if ($result->status == 1) {
                    return '<a href="#" onclick="change_status(this)" class="btn green btn-xs" data-toggle="modal">Enabled</a>';
                } else {
                    return '<a href="#" onclick="change_status(this)" class="btn red btn-xs" data-toggle="modal">Disabled</a>';
                }
            })
            ->add_column('action', '<button class="btn btn-default btn-xs" onclick="newsEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-default btn-xs" onclick="newsDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')
            ->make();
    }

    // change news status
    public function postChangenewsstatus()
    {
        $id = Input::get('id');
        $check = News::find($id);
        if ($check->status == 1) {
            News::where('id',$id)->update(array('status'=>0));
        } else {
            News::where('id',$id)->update(array('status'=>1));
        }
        return Response::json(array('status' => 'success'), 200);
    }

    public function postNewsdelete()
    {
        $id = Input::get('id');
        News::destroy($id);
        return Response::json(array('status' => 'success'), 200);
    }

    /* News Show */
    public function getNewsshow($id)
    {
        $news_content=News::find($id);
        $news_category=NewsCategory::where('id',$news_content->news_category_id)->first();
        $tags=explode(',',$news_content->tags);
        return View::make('admin/news_show')->with('news_content', $news_content)->with('news_category', $news_category)->with('tags',$tags);
    }

    public function getNewsedit($id)
    {
        $news_category = NewsCategory::all();
        return View::make('admin/news_edit')->with('news_id', $id)->with('news_category', $news_category);;
    }

    // get data for edit news
    public function postNewseditcontent()
    {
        $id = Input::get('id');
        $news_content = News::find($id);
        $news_content->response = 'success';
        return Response::json($news_content,200);
    }

    public function postUpdatenews()
    {
        $id=Input::get('id');
        News::where('id',$id)->update(
            array(
                'title'=>Input::get('title'),
                'description'=>Input::get('description'),
                'tags'=>Input::get('tags'),
                'status'=>Input::get('status'),
                'news_category_id'=>Input::get('news_category_id')
            )
        );

        return Response::json(array('response'=>'success'),200);
    }
}