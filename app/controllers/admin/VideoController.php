<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/21/2015
 * Time: 12:22 PM
 */
class VideoController extends BaseController
{
    public function getIndex()
    {
        return View::make('admin/video_album');
    }

    /**
     * get album list using datatables
     */
    public function postAlbumlist()
    {
        $albums = VideoAlbum::select(array('video_album.id','video_album.name',DB::raw('count(video_list.album_id) as total'),DB::raw('DATE_FORMAT(video_album.created_at,"%d-%b-%Y %H:%i:%s") as created'  ),DB::raw('DATE_FORMAT(video_album.updated_at,"%d-%b-%Y %H:%i:%s") as updated')))
            ->join('video_list', 'video_album.id', '=', 'video_list.album_id','left outer')->groupBy('video_album.id');

        return Datatables::of($albums)
            ->add_column('action', '<button class="btn btn-default btn-xs" onclick="albumEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-default btn-xs" onclick="albumDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')
            ->make();
    }

    /**
     * Create  Album
     */

    public function postAlbum()
    {
        $album_name=Input::get('name');
        VideoAlbum::create(array('name'=>$album_name));
    }

    /**
     * Delete Album
     */
    public function postDeletealbum()
    {
        $id=Input::get('id');
        VideoAlbum::destroy($id);
        $status = array('status'=>'success');
        echo json_encode($status);
    }

    /**
     * edit Album
     */
    public function postEditalbum()
    {
        $id = Input::get('id');
        $row = VideoAlbum::find($id)->toArray();
        $row['status']='success';

        echo json_encode($row);

    }
    /**
     * Update Album
     */
    public function postAlbumupdate()
    {
        $id = Input::get('id');
        VideoAlbum::where('id',$id)->update(array('name' => Input::get('name')));
    }

    /**
     * Images list
     */
    public function getVideolist($id)
    {
        $album = VideoAlbum::find($id);
        return View::make('admin/video_list')->with('album',$album);
    }

    /**
     * Show Videos
    */
    public function postShowvideos($id)
    {
        $images=Videos::select(array('id','title','link',DB::raw('DATE_FORMAT(created_at,"%d-%b-%y %H:%i:%s") as created'),DB::raw('DATE_FORMAT(updated_at,"%d-%b-%y %H:%i:%s") as updated')))->where('album_id',$id);
        return Datatables::of($images)->
        add_column('action', '<button class="btn btn-default btn-xs" onclick="videoEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-xs btn-default" onclick="videoDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')->make();
    }

    /**
     * add video
    */

    public function postAddvideo()
    {
        $link = Input::get('link');
        $url=parse_url($link);
        $video_id = explode('v=',$url['query']);

        Videos::create(array(
            'title' => Input::get('title'),
            'link' => end($video_id),
            'album_id' => Input::get('album_id'),
        ));

        return Response::json(array('status' => 'success'), 200);
    }

    /**
     * Edit Video
    */
    public function postEditvideo()
    {
        $id = Input::get('id');
        $video_detail = Videos::find($id);
        $video_detail->status='success';
        return Response::json($video_detail, 200);
    }

    /**
     * Update video
     */
    public function postUpdatevideo()
    {


        $title = Input::get('title');
        $id = Input::get('video_id');

        $link = Input::get('link');
        $url=parse_url($link);
        $video_id = explode('v=',$url['query']);

        Videos::where('id',$id)->update(
            array(
                'title'=>$title,
                'link'=>end($video_id)
            )
        );
        return Response::json(array('status' => 'success'), 200);
    }

    /**
     * Remove Video
     */
    public function postRemove()
    {
        $id = Input::get('id');
        Videos::Destroy($id);
        return Response::json(array('status' => 'success'), 200);
    }



}