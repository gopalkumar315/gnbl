<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 9/26/2015
 * Time: 4:55 PM
 */
class DownloadController extends BaseController
{

    public function postList()
    {
        $downloadList = Download::select(array('id','title','status','file',DB::raw('DATE_FORMAT(created_at,"%d-%b-%Y %H:%i:%s") as created')));
        return Datatables::of($downloadList)->make();
    }

    public function getIndex()
    {
        return View::make('admin/download');
    }

    public function postAdd()
    {
        $destinationPath = base_path().'\web\admin\uploads';
        $file = Input::file('file');
        $filename = trim(str_random(10).$file->getClientOriginalName());
        $file->move($destinationPath, $filename);

        Download::Create(array('title'=>Input::get('title'),'file'=>$filename,'status'=>1));

        $status=array('status'=>'success');
        return Response::json($status, 200);
    }

    public function postChangestatus()
    {
        $id=Input::get('id');
        $item_status = Download::find($id);
        if($item_status->status == 0){
            $status=1;
        } else {
            $status=0;
        }
        Download::where('id',$id)->update(array('status' => $status));

        $status=array('status'=>'success');
        return Response::json($status, 200);
    }

    public function postDelete()
    {
        $id=Input::get('id');
        $item = Download::find($id);
        /*Delete File*/
        $filename = base_path().'\web\admin\uploads\\'.$item->file;
        File::delete($filename);

        Download::destroy($id);
        return Response::json(array('status'=>'success'),200);
    }

    public function getFile($id,$filename='')
    {
        $item = Download::where(array('id'=>$id,'file'=>$filename))->first();
        if(empty($item)){
            echo 'No Result';
            exit;
        }
        $filename = base_path().'\web\admin\uploads\\'.$item->file;

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$item->title.'_'.$item->file."");
        header("Content-Transfer-Encoding: binary");
        header("Content-Type: binary/octet-stream");
        readfile($filename);
    }

    public function getDownload($id)
    {
        $item = Download::find($id);
        $filename = base_path().'\web\admin\uploads\\'.$item->file;

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$item->title.'_'.$item->file."");
        header("Content-Transfer-Encoding: binary");
        header("Content-Type: binary/octet-stream");
        readfile($filename);
    }
}