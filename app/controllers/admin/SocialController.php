<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 10/1/2015
 * Time: 3:53 PM
 */
class SocialController extends BaseController
{
    public function getIndex()
    {
        $social_list = Social::all();
        return View::make('admin/social_link')->with('social_list', $social_list);
    }

    public function getEdit()
    {

    }

    public function postEdit()
    {

    }

    public function postUpdate()
    {
        $postData = Input::all();
        unset($postData['/']);
        foreach($postData as $key => $value){
            $key=str_replace('_', ' ', $key);
            Social::where('name',$key)->update(array('link' => $value));
        }

    }
}