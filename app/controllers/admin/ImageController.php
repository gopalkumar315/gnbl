<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/8/15
 * Time: 9:02 PM
 */

class ImageController extends BaseController
{
    public function getIndex()
    {
        return View::make('admin/album');
    }


    /**
     * get album list using datatables
    */
    public function postAlbumlist()
    {
        $albums = Album::select(array('albums.id','albums.name',DB::raw('count(photos.album_id) as total'),DB::raw('DATE_FORMAT(albums.created_at,"%d-%b-%Y %H:%i:%s") as created'  ),DB::raw('DATE_FORMAT(albums.updated_at,"%d-%b-%Y %H:%i:%s") as updated')))
            ->join('photos', 'albums.id', '=', 'photos.album_id','left outer')->groupBy('albums.id');

        return Datatables::of($albums)
            ->add_column('action', '<button class="btn btn-default btn-xs" onclick="albumEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-default btn-xs" onclick="albumDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')
            ->make();
    }

    /**
    * Create  Album
    */

    public function postAlbum()
    {
        $album_name=Input::get('name');
        Album::create(array('name'=>$album_name));
    }


    /**
     * Delete Album
    */
    public function postDeletealbum()
    {
        $id=Input::get('id');
        Album::destroy($id);

        $status = array('status'=>'success');
        echo json_encode($status);
    }


    /**
     * edit Album
    */
    public function postEditalbum()
    {
        $id = Input::get('id');
        $row = Album::find($id)->toArray();
        $row['status']='success';

        echo json_encode($row);

    }

    public function postAlbumupdate()
    {
        $id = Input::get('id');
        Album::where('id',$id)->update(array('name' => Input::get('name')));
    }

    /**
     * Images list
    */
    public function getImagelist($id)
    {
        $album = Album::find($id);
        return View::make('admin/images')->with('album',$album);
    }

    public function postShowimages($id)
    {
        $images=Photos::select(array('id','title','file',DB::raw('DATE_FORMAT(created_at,"%d-%b-%y %H:%i:%s") as created'),DB::raw('DATE_FORMAT(updated_at,"%d-%b-%y %H:%i:%s") as updated')))->where('album_id',$id);
        return Datatables::of($images)->
            add_column('action', '<button class="btn btn-default btn-xs" onclick="imageEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn btn-default btn-xs" onclick="imageDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')->make();
    }

    public function postUpload()
    {
        $destinationPath = base_path().'\web\admin\uploads';
        $file = Input::file('file');
        $filename = trim(str_random(10).$file->getClientOriginalName());
        $file->move($destinationPath, $filename);

        $ImageDetail=Photos::Create(
            array('album_id'=>Input::get('id'),'file'=>$filename,'title'=>Input::get('title')
                ));
        $status=array(
            'status'=>'success',
            'id'=>$ImageDetail->id
        );
        return Response::json($status, 200);
    }

    public function postRemove()
    {
        $id=Input::get('id');
        $content = Photos::find($id);
        /*Delete File*/
        $filename = base_path().'\web\admin\uploads\\'.$content->file;
        File::delete($filename);

        Photos::destroy($id);
        return Response::json(array('status'=>'success'),200);
    }

    public function postEdit()
    {
        $id=Input::get('id');
        $update_detail = Photos::find($id);
        $update_detail->status = 'success';
        return Response::json($update_detail, 200);
    }

    /* Image Update */
    public function postUpdate()
    {
        $id=Input::get('id');
        $imageDetail = Photos::find($id);
        $filename=$imageDetail->file;
        $title=$imageDetail->title;

        if (Input::hasFile('file')) {

            /*Delete Previous file*/
            $filename = base_path().'\web\admin\uploads\\'.$imageDetail->file;
            File::delete($filename);

            $destinationPath = base_path().'\web\admin\uploads';
            $file = Input::file('file');
            $filename = trim(str_random(10).$file->getClientOriginalName());
            $file->move($destinationPath, $filename);
        }

        if (Input::has('title')) {
            $title = Input::get('title');
        }

        Photos::where('id',Input::get('id'))->update(
            array(
                'title'=>$title,
                'file'=>$filename
            )
        );

        return Response::json(array('status' => 'success'), 200);
    }
}
