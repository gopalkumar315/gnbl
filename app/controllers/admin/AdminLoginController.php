<?php

class AdminLoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        return View::make('admin.login');
	}


	/**
	 * Login the admin
	 *
	 * @return Response
	 */
	public function postLogin()
	{
        $admin_login=array(
            'username'=>Input::get('username'),
            'password'=>Input::get('password'),
            'is_superuser'=>1
        );

        if(Auth::admin()->attempt($admin_login)) {
            return Redirect::to('admin/index');
        } else {
            return Redirect::back()->with('failure','Invalid Username and Password.');
        }

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
     * Forgot password
    */

    public function postForgot()
    {
        $email = Input::get('email');
        $admin_content=User::where('email',$email)->where('is_superuser','1')->first();
        if (count($admin_content) > 0) {

            $content=array(
                'email'=>$admin_content->email
            );

            $confirmation_code=str_random(50);

            $admin=User::find($admin_content->id);
            $admin->reset_code=$confirmation_code;
            $admin->save();

            $confirmation=array(
                'confirmation_code'=>$confirmation_code,
                'user_id'=>$admin->id,
                'username'=>$admin->username
            );

            Mail::send('emails.admin_reset_password',$confirmation, function($message) use ($content)
            {
                $message->to($content['email'], 'Hello Admin')->subject('Reset Password!');
            });

            echo "1";
        } else {
            echo "0";
        }


    }

    /**
     * Admin Logout
    */

    public function getLogout() {

        Auth::admin()->logout();
        return Redirect::to('/');
    }

    public function getVerify($code,$user_id){
        $verify = array(
            'user_id'=>$user_id,
            'code'=>$code
        );

        $user_exits = User::where('reset_code', $code)->where('id',$user_id)->count();
        if($user_exits>0){
            return View::make('admin.admin_reset_password')->with('verify',$verify);
        } else {
            return Redirect::to('admin/login')->with('failure','Reset link has expired.');
        }
    }

    public function postReset()
    {
        $code=Input::get('code');
        $user_id=Input::get('id');
        $password=Input::get('password');

        $user_reset=User::where('reset_code', $code)->where('id',$user_id)->first();
        if(count($user_reset)>0) {
            $user_reset->password=Hash::make($password);
            $user_reset->reset_code='';
            $user_reset->save();

            return Redirect::to('admin/login')->with('success','Password has reset.');
        }
    }

}
