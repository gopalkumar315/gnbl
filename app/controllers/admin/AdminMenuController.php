<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/26/2015
 * Time: 9:05 PM
 */
class AdminMenuController extends BaseController
{
    public function getIndex()
    {
        return View::make('admin/menu');
    }

    /**
     * get menu list using datatables
     */
    public function postMenulist()
    {
        $albums = Menu::select(array('menus.id','menus.name',DB::raw('DATE_FORMAT(menus.created_at,"%d-%M-%Y %H:%i:%s") as created'  ),DB::raw('DATE_FORMAT(menus.updated_at,"%d-%M-%Y %H:%i:%s") as updated')))
            ->where('parent_id',0);

        return Datatables::of($albums)
            ->add_column('count',function($data){
                return Menu::where('parent_id',$data->id)->count();
            },2)
            ->add_column('action', '<button class="btn green btn-sm" onclick="menuEdit(this)"> <i class="fa fa-pencil-square-o"></i> Edit </button> <button class="btn red btn-sm" onclick="menuDelete(this)"> <i class="fa fa-trash-o"></i> Delete </button>')
            ->make();
    }

    /**
     * Create  Menu
     */

    public function postMenu()
    {
        $album_name=Input::get('name');
        Menu::create(array('name'=>$album_name));
        return Response::json(array('status' => 'success'), 200);
    }


    /**
     * Delete menu
     */
    public function postDeletemenu()
    {
        $id=Input::get('id');
        Menu::destroy($id);
        $status = array('status'=>'success');
        echo json_encode($status);
    }


    /**
     * edit Menu
     */
    public function postEditmenu()
    {
        $id = Input::get('id');
        $menu_content = Menu::find($id);
        $menu_content->status = 'success';
        return Response::json($menu_content);
    }

    /* Menu Update */
    public function postMenuupdate()
    {
        $id = Input::get('id');
        Menu::where('id',$id)->update(array('name' => Input::get('name')));
    }


}