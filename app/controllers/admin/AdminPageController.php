<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/26/2015
 * Time: 9:06 PM
 */
class AdminPageController extends BaseController
{

    protected $sub_cat;

    public function getIndex()
    {
        $list=$this->get_categories();
        return View::make('admin/pages')->with('categories',$list);
    }

    public function getAddpage()
    {

        $list=$this->get_categories();
        return View::make("admin/page_add")->with('categories',$list);
    }

    public function postAddpage()
    {
        $input = Input::all();
        $destinationPath = base_path().'\web\admin\uploads';
        $file = Input::file('file');
        $filename = trim(str_random(10).$file->getClientOriginalName());
        $file->move($destinationPath, $filename);

        $input['banner'] = $filename;
        $input['page_label'] = str_replace('/', '-', $input['page_label']);
        unset($input['file']);
        unset($input['/']);
        Pages::create($input);
        return Response::json(array('status' => 'success'), 200);
    }



    public function get_category_pages($id,$level='-')
    {
        $parent=Pages::where('parent_id', $id)->get(array('id','parent_id','page_label','status'))->toArray();
        $level.=$level;
        foreach ($parent as $row) {
            $row['page_label']=$level.''.$row['page_label'];
            $this->sub_cat[] = $row;

            $this->get_category_pages($row['id'],$level);
        }
        return $this->sub_cat;
    }

    public function get_categories()
    {
        $parent = Pages::where('parent_id',0)->get(array('id','parent_id','page_label','status'));//->toArray();
        foreach ($parent as $row) {
            $this->sub_cat = array();
            $row->level=$this->get_category_pages($row->id);
        }
        $list=$parent->toArray();
        return $list;
    }

    public function get_breadcrumb($id)
    {
        $parent=Pages::where('id', $id)->get(array('id','parent_id','page_label','status'))->toArray();
        foreach ($parent as $row) {
            $this->sub_cat[] = $row;
            $this->get_breadcrumb($row['parent_id']);
        }
        return $this->sub_cat;
    }

    public function getShowpage($id){
        $page_detail = Pages::find($id);
        $page_detail->tags=explode(',',$page_detail->tags);
        $breadcrumbs = $this->get_breadcrumb($page_detail->parent_id);
        if(empty($breadcrumbs)){
            $breadcrumbs = array();
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        return View::make('admin/page_show')->with('detail',$page_detail)->with('breadcrumb',$breadcrumbs);
    }



    public function postChangestatus()
    {
        $id = Input::get('id');
        $page_detail=Pages::find($id);
        if($page_detail->status == 0){
            $status=1;
        } else {
            $status=0;
        }

        Pages::where('id', $id)->update(array(
            'status'=>$status
        ));

        return Response::json(array('status' => 'success'), 200);
    }

    public function postDelete()
    {
        $id = Input::get('id');
        $page_detail = Pages::find($id);
        if($page_detail->parent_id!=0){
            Pages::where('parent_id',$id)->update(
                array(
                    'parent_id'=>  $page_detail->parent_id
                )
            );
        }
        Pages::destroy($id);
        return Response::json(array('status' => 'success'), 200);
    }

    public function getEdit($id)
    {
        $list=$this->get_categories();
        return View::make('admin/page_edit')->with(array('page_id' => $id))->with('categories',$list);;
    }

    public function postEditpage()
    {
        $id = Input::get('id');
        $page_detail = Pages::find($id);
        $page_detail->response_status='success';
        return Response::json($page_detail, 200);
    }

    public function postUpdatepage()
    {
        $input = Input::all();
        $id = $input['id'];

        if(Input::hasFile('file')){
            $destinationPath = base_path().'\web\admin\uploads';
            $file = Input::file('file');
            $filename = trim(str_random(10).$file->getClientOriginalName());
            $file->move($destinationPath, $filename);
            $input['banner'] = $filename;
        } else {
            unset($input['banner']);
        }
        unset($input['id']);
        unset($input['file']);
        unset($input['/']);

        Pages::where('id', $id)->update($input);
        return Response::json(array('status' => 'success'), 200);
    }
}