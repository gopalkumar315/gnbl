<?php

class UserloginController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | User Login Controller
    |--------------------------------------------------------------------------
    |
    */

    public function getIndex()
    {
        return View::make('user.login');
    }

    /**
     * Authenticate the user
    */

    public function postAuth()
    {
        $user_login=array(
            'username'=>Input::get('username'),
            'password'=>Input::get('password'),
            'is_superuser'=>2
        );

        if (Auth::user()->attempt($user_login)) {

            $set_session=User::find(Auth::user()->get()->id);
            $set_session->session_id=Session::getId();
            $set_session->save();

            return Redirect::to('user/index');
        } else {
            return Redirect::back()->with('failure','Invalid Username and Password');
        }
    }

    /**
     * Logout
    */

    public function getLogout()
    {
        Auth::user()->logout();
        return Redirect::to('user/login');

    }

}
