<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/1/15
 * Time: 11:04 PM
 */

class UserIndexController extends BaseController{

    public function getIndex()
    {
        return View::make('user.index');
    }
}