<?php
/**
 * Created by PhpStorm.
 * User: GOPAL KUMAR
 * Date: 9/22/2015
 * Time: 6:31 PM
 */

class FrontController extends BaseController {


    protected $sub_cat;
    public function getIndex()
    {
        $data = array();
        $data['principal_message'] = Featured::find(1);
        $data['news'] = NewsCategory::orderBy('id', 'DESC')->get();
        $data['albums'] = Album::limit(8)->orderBy('id','desc')->get();
        return View::make('front/index')->with('data',$data);
    }

    public function postNavmenu()
    {
        $list=$this->get_categories();
        return Response::json($list);
    }

    public function postNewslist()
    {
        $id = Input::get('id');
        if($id == 0){
            $id = NewsCategory::max('id');
        }
        $list=News::where(array('status' => 1,'news_category_id' => $id))->limit(5)->get(array('id','title','news_category_id',DB::raw('DATE_FORMAT(created_at,"%d-%b-%Y %h:%i:%s") as date_time')));
        return Response::json($list, 200);
    }

    public function get_category_pages($id)
    {
        $parent=Pages::where(array(
            'parent_id'=> $id,
            'status'=>1
        ))->get(array('id','page_label'))->toArray();
        return $parent;
    }


    public function get_categories()
    {
        $parent = Pages::where(
            array(
                'parent_id'=>0,
                'status'=>1
            )
        )->get(array('id','parent_id','page_label','status'));//->toArray();
        foreach ($parent as $row) {
            $this->sub_cat = array();
            $row->level=$this->get_category_pages($row->id);
        }
        $list=$parent->toArray();
        return $list;
    }

    public function getOtherpage($id,$name)
    {
        $content=Featured::where(array(
            'id'=>$id,
            'page_label'=>$name
        ))->first();

        $menus = Pages::where(array(
            'parent_id' => 0,
            'status' => 1
        ))->get();
        return View::make('front/other_page')->with('content', $content)->with('menus',$menus);
    }

    public function getPage($id,$name)
    {
        $content=Pages::where(array(
            'id'=>$id,
            'page_label'=>$name,
            'status'=>1
        ))->first();

        $data['breadcrumb'] = array_reverse($this->get_breadcrumb($content->id));
        $data['root'] = $data['breadcrumb'][0];
        if($content->parent_id == 0){
             $data['parent_pages'] = Pages::where('parent_id', $content->id)->get(array('id','page_label'));
        } else {
            $root_id=$data['breadcrumb'][0]['id'];
            $data['parent_pages'] = $data['breadcrumb'][0];
            $data['parent_pages']  = Pages::where('parent_id', $root_id)->get(array('id','page_label'));
            $data['sub_pages'] = Pages::where('parent_id', $content->id)->get(array('id','page_label'));
        }
        return View::make('front/page')->with('content', $content)->with('data',$data);
    }


    public function get_breadcrumb($id)
    {
        $parent=Pages::where('id', $id)->get(array('id','parent_id','page_label','status'))->toArray();
        foreach ($parent as $row) {
            $this->sub_cat[] = $row;
            $this->get_breadcrumb($row['parent_id']);
        }
        return $this->sub_cat;
    }

    public function getAllnews($id)
    {
        $news['news_sidebar'] = $this->news_sidebar($id);
        $news['list'] = News::where(
            array(
                'status' => 1,
                'news_category_id' => $id
            )
        )->orderBy('id', 'desc')->limit(10)->get();
        $news['news_category'] = NewsCategory::find($id);
        return View::make('front/all_news')->with('news',$news);
    }

    public function getShownews($id,$title){
        $news['content'] = News::where(array(
            'id'=>$id,
            'title' => $title
        ))->first();
        $news['news_category'] = NewsCategory::find($news['content']->news_category_id);
        $news['news_sidebar'] = $this->news_sidebar($news['content']->news_category_id);
        return View::make('front/shownews')->with('news',$news);
    }

    public function getNewslist($id,$month,$year)
    {
        $news['news_sidebar'] = $this->news_sidebar($id);
        $news['news_category'] = NewsCategory::find($id);
        $news['list'] = News::where(
            array(
                'news_category_id'=>$id,
                'status' => 1,
            )
        )->where(DB::raw('year(created_at)'),$year)->where(DB::raw('DATE_FORMAT(created_at,"%M")'),strtoupper($month))->orderBy('id')->get();

        return View::make('front/newslist')->with('news',$news);
    }

    public function news_sidebar($news_category_id)
    {
        return News::select(array(DB::raw('count(*) as total'),DB::raw('year(created_at) as year'),DB::raw('DATE_FORMAT(created_at,"%M") as month')))
            ->where(array(
                'news_category_id' => $news_category_id,
                'status' => 1
            ))->groupBy(array(DB::raw('year(created_at)')))->groupBy(array(DB::raw('month(created_at)')))->orderBy(DB::raw('month(created_at)'),'desc')->get();
    }

    public function getAlbums()
    {
        $data['albums'] = Album::orderBy('id','desc')->get();
        return View::make('front/photo_album')->with('data',$data);
    }

    public function getAlbumimages($id, $album)
    {
        $data['albums'] = Album::orderBy('id','desc')->get();
        $data['album'] = Album::find($id);
        $data['images'] = Photos::where(
            array(
                'album_id' => $id,
            )
        )->orderBy('id','desc')->get();
        return View::make('front/images')->with('data',$data);
    }

    public function getVideoalbum() {
        $data['albums'] = VideoAlbum::orderBy('id','desc')->get();
        return View::make('front/video_album')->with('data',$data);
    }

    public function getVideolist($id,$name)
    {
      $data['albums'] = VideoAlbum::orderBy('id','desc')->get();
      $data['album'] = VideoAlbum::find($id);
      $data['images'] = Videos::where(
          array(
              'album_id' => $id,
          )
      )->orderBy('id','desc')->get();
      return View::make('front/video_list')->with('data',$data);
    }

    public function getContactus()
    {
        return View::make('front/contact_us');
    }

    public function getImage()
    {
        $img_path = $uri = Request::path();
        $img_path = str_replace('image/', '', $img_path);

        $real_path = base_path() . '/' . $img_path;
        if(!file_exists($real_path)){
            $img_path = "web/front/img/default.jpg";

        }

        $query = parse_url($_SERVER['REQUEST_URI']);
        $query = $query['query'];
        $query = str_replace('s=', '', $query);
        list($width, $height) = explode('x', $query);

        $image_moo=new ImageMoo;
        $image = $image_moo->load($img_path)->resize_crop($width,$height)->save_dynamic();
    }

    public function postFootermenus()
    {
        $data['menuBar'] = Pages::where(
            array(
                'parent_id'=>0,
                'status'=>1
            )
        )->limit(5)->get(array('id','parent_id','page_label','status'));

        $data['newsList'] = NewsCategory::orderBy('id', 'DESC')->limit(5)->get();
        $data['socialLinks'] = Social::orderBy('id', 'asc')->get();
        return Response::json($data,200);
    }



}
