<?php


class Album extends Eloquent {



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'albums';
    protected $guarded = array('id');

    public function photos()
    {
        return $this->hasOne('Photos','album_id','id')->orderBy('created_at', 'DESC');;
    }

}























