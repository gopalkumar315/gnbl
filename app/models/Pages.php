<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 7/26/2015
 * Time: 9:09 PM
 */

class Pages extends Eloquent
{
    protected $table = 'pages';
    protected $guarded = array('id');

    public function sub_categories()
    {
        return $this->hasMany('Pages','parent_id');
    }
}