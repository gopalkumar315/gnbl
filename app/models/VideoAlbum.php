<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class VideoAlbum extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     * @var string
     */

    protected $table = 'video_album';
    protected $guarded = array('id');

    public function videos()
    {
        return $this->hasOne('Videos','album_id','id');
    }
}