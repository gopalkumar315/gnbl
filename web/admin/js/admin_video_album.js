/**
 * Created by john on 7/8/15.
 */
AdminVideosAlbum = function() {

    var oTable ;
    var initTable = function () {

        var table = $('#album_table');

        /* Fixed header extension: http://datatables.net/extensions/keytable/ */
        var baseURL = $('#baseUrl').data('baseurl');

        oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            serverSide: true,

            ajax: {
                url: baseURL + '/admin/videos/albumlist',
                type: 'POST'
            },

            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            "columnDefs": [
                {
                    // set default column settings
                    'orderable': false,
                    'targets': [5]
                },
                {
                    "searchable": false,
                    "targets": [2]
                },
                {
                    "render": function ( data, type, row ) {

                        if (row[2] > 1) {
                            return '<a href="' + baseURL + '/admin/videos/videolist/'+row[0]+'">' + data + ' Videos</a>';
                        } else {
                            return '<a href="' + baseURL + '/admin/videos/videolist/'+row[0]+'">' + data + ' Video</a>';
                        }

                    },
                    "targets": 2
                }

            ],
            "fnDrawCallback": function (oSettings) {

            }
        });


        var oTableColReorder = new $.fn.dataTable.ColReorder(oTable);
        var tableWrapper = $('#album_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

    }


    /*Add Album*/
    addAlbum=function() {
        var baseURL = $('#baseUrl').data('baseurl');
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var form = $('#add_album_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);


        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                name: {
                    required: true
                }

            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/videos/album",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Album Created");
                        //Toaster code ends here
                        jQuery('#btn_close_add_model').click();
                        form.reset();
                        oTable._fnAjaxUpdate();

                    },
                    error: function () {
                        toastr['error']("Error", "Something went wrong");
                    }
                });
                return false;
            }

        });
    }

    albumDelete = function(target){
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        bootbox.confirm('Do you really want to delete this album?', function(result){
            if(result == true) {
                $.post(baseURL+"/admin/videos/deletealbum",{id:id},function(data){
                    if(data.status=='success') {
                        toastr['success']('Album Deleted Successfully');
                        oTable._fnAjaxUpdate();
                    } else {
                        toastr['error']('Error In Deletion');
                        oTable._fnAjaxUpdate();
                    }
                },'json');
            }
        });
    }

    albumEdit = function (target) {
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        var form=$('#edit_album_form');
        $.post(baseURL+"/admin/videos/editalbum",{id:id},function(data){
            if(data.status='success') {
                $('[name="name"]',form).val(data.name);
                $('[name="id"]',form).val(data.id);
                jQuery('#editAlbum').modal('show');
            }
        },'json')
    };

    albumUpdate = function () {
        var baseURL = $('#baseUrl').data('baseurl');

        var form = $('#edit_album_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);


        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                name: {
                    required: true
                }

            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/videos/albumupdate",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Album Updated");
                        //Toaster code ends here
                        jQuery('#editAlbum').modal('hide');
                        form.reset();
                        oTable._fnAjaxUpdate();
                    },
                    error: function () {
                        toastr['error']("Error", "Something went wrong");
                    }
                });
                return false;
            }
        });
    };


    return {
        init: function() {
            initTable();
            albumUpdate();
            addAlbum();

        }
    };
}();