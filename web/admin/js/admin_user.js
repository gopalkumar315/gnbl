/**
 * Created by john on 6/29/15.
 */
var AdminUser = function () {

    var initTable = function () {

        var table = $('#admin_user');

        /* Fixed header extension: http://datatables.net/extensions/keytable/ */
        var baseURL = $('#baseUrl').data('baseurl');

        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            serverSide: true,

            ajax: {
                url: baseURL + '/mylove007/user/users',
                type: 'POST'
            },

            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            "columnDefs": [
                {  // set default column settings
                    'orderable': true,
                    'targets': [0]
                },
                {
                    "searchable": false,
                    "targets": [0]
                }
            ],
            "fnDrawCallback": function (oSettings) {
                user_edit();
                delete_user();
            }
        });


        var oTableColReorder = new $.fn.dataTable.ColReorder(oTable);
        var tableWrapper = $('#admin_user_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

    }


    /*Add User form Validate*/
    var add_user_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var form = $('#add_user_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);


        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                username: {
                    required: true,
                    remote: {
                        url: baseURL + "/mylove007/user/validator",
                        type: "post",
                        data: {
                            username: function () {
                                return $("#username").val();
                            },
                            rules: function () {
                                return "unique:users";
                            }
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: baseURL + "/mylove007/user/validator",
                        type: "post",
                        data: {
                            email: function () {
                                return $("#add_email").val();
                            },
                            rules: function () {
                                return "unique:users";
                            }
                        }
                    }
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                gender: {
                    required: true
                },
                mobile: {
                    required: true,
                    number: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                retype_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                police_station: {
                    required: true
                },
                district: {
                    required: true
                },
                dob: {
                    required: true
                }

            },

            messages: { // custom messages for radio buttons and checkboxes
                email: {
                    remote: "Email address already in use. Please use other email."
                },
                username: {
                    remote: "Username address already in use"
                }
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
            //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/mylove007/user/adduser",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        Command: toastr['success']("Successfull", "User Account Successfully Created")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "1000",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        //Toaster code ends here
                        jQuery('#btn_close_model').click();
                        form.reset();
                        $('#admin_user').dataTable().fnDestroy();
                        initTable();

                    },
                    error: function () {
                        alert('failed');
                    }
                });
                return false;
            }

        });

    }

    function user_edit() {

        var baseURL = $('#baseUrl').data('baseurl');
        $('.user_edit').on('click', function () {
            var id = $(this).attr('id');

            $.ajax({
                type: "post",
                url: baseURL + "/mylove007/user/edit",
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if(data.status==1) {
                        $('#edit_username').val(data.username);
                        $('#edit_email').val(data.email);
                        $('#edit_first_name').val(data.user_first_name);
                        $('#edit_last_name').val(data.user_last_name);
                        $('#edit_gender').val(data.gender);
                        $('#edit_dob').val($.datepicker.formatDate('dd-mm-yy', new Date(data.dob)));
                        $('#edit_mobile').val(data.mobile);
                        $('#edit_police_station').val(data.police_station);
                        $('#edit_district').val(data.district);
                        $('#user_id').val(data.id);
                    }
                }
            });
        })
    }



    /*Add User form Validate*/
    var edit_user_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var form = $('#edit_user_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);


        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                username: {
                    required: true,
                    remote: {
                        url: baseURL + "/mylove007/user/editvalidator",
                        type: "post",
                        data: {
                            username: function () {
                                return $("#edit_username").val()+"_"+$("#user_id").val();
                            },
                            rules: function () {
                                return "unique:users";
                            }
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: baseURL + "/mylove007/user/editvalidator",
                        type: "post",
                        data: {
                            email: function () {
                                return $("#edit_email").val()+"_"+$("#user_id").val();
                            },
                            rules: function () {
                                return "unique:users";
                            }
                        }
                    }
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                gender: {
                    required: true
                },
                mobile: {
                    required: true,
                    number: true
                },
                police_station: {
                    required: true
                },
                district: {
                    required: true
                },
                dob: {
                    required: true
                },
                retype_password: {
                    equalTo: "#edit_password"
                },

            },

            messages: { // custom messages for radio buttons and checkboxes
                email: {
                    remote: "Email address already in use. Please use other email."
                },
                username: {
                    remote: "Username address already in use"
                }
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
               // Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/mylove007/user/updateuser",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        Command: toastr['success']("Edited", "User Account Successfully Edited")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "1000",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        //Toaster code ends here
                        jQuery('#btn_close_edit_model').click();
                        form.reset();
                        $('#admin_user').dataTable().fnDestroy();
                        initTable();

                    },
                    error: function () {
                        alert('failed');
                    }
                });
                return false;
            }
        });
    }

    /*Delete User*/
    delete_user= function () {

        $('.user_delete').on('click', function () {
            var user_id=$(this).attr('id');
            $('#delete_user_id').val(user_id);
        });
    };

    delete_user_confirm = function (){

        $('.delete_user_confirm').on('click', function () {
            var user_id=$('#delete_user_id').val();
            var baseURL = $('#baseUrl').data('baseurl');

            $.ajax({
               type:"post",
               url:baseURL+"/mylove007/user/destroy",
               data:{user_id:user_id},
                success: function () {

                    Command: toastr['success']("Deleted", "User Successfully Deleted")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    //Toaster code ends here
                    $('#admin_user').dataTable().fnDestroy();
                    initTable();

                },
                error: function () {

                    Command: toastr['danger']("Error", "Error In Deletion")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                }

            });
        });
    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            console.log('Admin User 1');

            initTable();
            add_user_validation();
            edit_user_validation();
            delete_user_confirm();

            console.log('Admin User 2');
        }
    };

}();

