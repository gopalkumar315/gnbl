/**
 * Created by john on 7/22/2015.
 */

AdminNewsCategory =function() {

    var oTable;
    initTable = function () {
        var baseURL = $('#baseUrl').data('baseurl');
        var table = $('#news_category_table');

        oTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            serverSide: true,
            ajax: {
                url: baseURL + '/admin/news/newscategorylist',
                type: 'POST'
            },
            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            columnDefs: [
                {  // set default column settings
                    'orderable': false,
                    'targets': [3,6]
                },
                {
                    "searchable": false,
                    "targets": [0,6,3,2]
                },
                {
                    "render": function (data, type, row) {
                        return '<a href="' + baseURL + '/admin/news/newslist/' + row[0] + '">' + data + ' News</a>';
                    },
                    "targets": 2
                },

                {
                    "render": function (data, type, row) {
                        return '<i class="'+row[3]+'"></i>';
                    },
                    "targets": 3
                }
            ]
        });

        var tableWrapper = $('#news_category_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };

    /* add news category */
    /*Add User form Validate*/
    var add_news_category_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        var form = $('#add_news_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                name: {
                    required: true,
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/news/addnewscategory",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "News Category Added")
                        //Toaster code ends here
                        jQuery('#addNews').modal('hide');
                        form.reset();
                        oTable._fnAjaxUpdate();
                    },
                    error: function () {
                        toastr['error']("error", "Something went wrong")
                    }
                });
                return false;
            }

        });
    }

    newsDelete = function (target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        bootbox.confirm('Do you really want to delete this news category ?', function (result) {
            if (result == true) {
                $.post(baseURL + '/admin/news/deletecategory', {id: id}, function (data) {
                    if (data.status == 'success') {
                        toastr['success']("Successfully", "News Category Deleted");
                        oTable._fnAjaxUpdate();
                    }
                })
            }
        });
    };

    newsEdit = function (target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var form = $('#updateForm');

        var baseURL = $('#baseUrl').data('baseurl');
        $.post(baseURL + '/admin/news/editnewscategory', {id: id}, function (data) {
            $('[name="name"]',form).val(data.name);
            $('[name="id"]', form).val(data.id);
            $('[name="icon"]',form).val(data.icon);
            jQuery('#editNews').modal('show');
        })
    };

    newsUpdate = function () {
        var baseURL = $('#baseUrl').data('baseurl');
        var form = $('form#updateForm');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
              name: {
                    required: true,
                },

            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    url: baseURL+'/admin/news/updatenewscategory',
                    type: "POST",             // Type of request to be send, called as method
                    data: $(form).serialize(),
                    dataType:'json',
                    success:function(data){
                        if(data.status=='success') {
                            oTable._fnAjaxUpdate();
                            document.getElementById("updateForm").reset();
                            jQuery('#editNews').modal('hide');
                            toastr['success']("Successfully", "News Category Updated");
                        } else {
                            toastr['error']("Error", "Something went wrong");
                        }
                    }
                });
                return false;
            }

        });
    }


    return {
        init:function() {
            initTable();
            add_news_category_validation();
            newsUpdate();
        }
    }
}();