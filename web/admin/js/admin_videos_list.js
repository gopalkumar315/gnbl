/**
 * Created by john on 7/18/2015.
 */
AdminVideoList = function () {

    /* DataTable */
    var oTable;
    var initTable = function () {

        var table = $('#video_list_table');
        var baseURL = $('#baseUrl').data('baseurl');
        var albumId = $('#albumId').data('value');

        oTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            serverSide: true,
            ajax: {
                url: baseURL + '/admin/videos/showvideos/' + albumId,
                type: 'POST'
            },
            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [1,2,5]
                },
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "render": function ( data, type, row ) {
                        return '<a href="https://www.youtube.com/watch?v='+row[2]+'" class="fancybox"> <img src="http://img.youtube.com/vi/'+row[2]+'/default.jpg" style="height:50px; width:50px;"></a>';
                    },
                    "targets": 2
                }
            ],
            "fnDrawCallback": function ( oSettings ) {
                handleFancybox();
            },
        });
        var tableWrapper = $('#video_list_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };



    /*Add User form Validate*/
    var add_video_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        var form = $('#add_video_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                title: {
                    required: true,
                },
                link: {
                    required: true,
                    url: true
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/videos/addvideo",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Video Added")
                        //Toaster code ends here
                        jQuery('#addVideo').modal('hide');
                        form.reset();
                        oTable._fnAjaxUpdate();
                    },
                    error: function () {
                        toastr['error']("error", "Something went wrong")
                    }
                });
                return false;
            }

        });

    }


    var edit_video_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        var form = $('#edit_video_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                title: {
                    required: true,
                },
                link: {
                    required: true,
                    url: true
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/videos/updatevideo",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Video Updated")
                        //Toaster code ends here
                        jQuery('#editVideo').modal('hide');
                        form.reset();
                        oTable._fnAjaxUpdate();
                    },
                    error: function () {
                        toastr['error']("error", "Something went wrong")
                    }
                });
                return false;
            }

        });

    }


    videoEdit=function(target){
        var baseURL = $('#baseUrl').data('baseurl');
        var form=$('#edit_video_form');
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        $.post(baseURL + '/admin/videos/editvideo', {id: id}, function (data) {
            if(data.status=='success'){
                $('#editVideo').modal('show');
                $('[name="link"]',form).val('https://www.youtube.com/watch?v='+data.link);
                $('[name="title"]',form).val(data.title);
                $('[name="video_id"]',form).val(data.id);
                $('#iframe').attr('src','http://www.youtube.com/embed/'+data.link)
            }
        }, 'json');
    };

    videoDelete = function(target) {
        var baseURL = $('#baseUrl').data('baseurl');
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Do you really want to delete this Video? ',function(result){
            if(result== true) {
                $.post(baseURL+'/admin/videos/remove',{id:id},function(data){
                    if(data.status == 'success'){
                        oTable._fnAjaxUpdate();
                        toastr['success']("Successfully", "Video Deleted");
                    } else {
                        toastr['error']("error", "Error in Deletion");
                    }

                },'json');
            }
        }).off("shown.bs.modal");
    };

    var handleFancybox = function() {
        $(".fancybox").click(function() {
            $.fancybox({
                'padding'		: 0,
                'autoScale'		: false,
                'transitionIn'	: 'none',
                'transitionOut'	: 'none',
                'title'			: this.title,
                'width'			: 640,
                'height'		: 385,
                'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type'			: 'swf',
                'swf'			: {
                    'wmode'				: 'transparent',
                    'allowfullscreen'	: 'true'
                }
            });

            return false;
        });
    };

    return {
        init: function () {
            initTable();
            add_video_validation();
            edit_video_validation();
        }
    };
}();

