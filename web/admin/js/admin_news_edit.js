/**
 * Created by john on 7/23/2015.
 */
AdminEditNews=function() {

    /*Add User form Validate*/
    var edit_news_validation = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        var form = $('#edit_news_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                title: {
                    required: true,
                },
                news_category_id: {
                    required:true,
                },
                status: {
                    required:true,
                },
                description: {
                    required: function()
                    {
                        CKEDITOR.instances.description.updateElement();
                    }
                }
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/news/updatenews",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                         toastr['success']("Successfully", "News Updated");
                    },
                    error: function () {
                        toastr['error']("error", "Something went wrong")
                    }
                });
                return false;
            }

        });

        $('.select2me', form).change(function () {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    }

    var handleTagsInput = function () {
        if (!jQuery().tagsInput) {
            return;
        }
        $('#tags_1').tagsInput({
            width: 'auto',
            'onAddTag': function () {
                //alert(1);
            },
        });

    }

    get_editable_content=function() {

        var id= $('#newsId').data('value');
        var form = $('#edit_news_form');
        var baseURL = $('#baseUrl').data('baseurl');
        $.post(baseURL+'/admin/news/newseditcontent',{id:id},function(data){
            if(data.response == 'success'){
                $('[name="title"]',form).val(data.title);
                $('[name="news_category_id"]', form).select2("val",data.news_category_id);
                CKEDITOR.replace( 'description');
                CKEDITOR.instances.description.setData(data.description);
                $('#new_title').text(data.title);
                $('[name="status"]', form).select2("val",data.status);
                $('[name="tags"]').importTags(data.tags);
            }

        },'json');
    };


    return {
        init: function () {
            edit_news_validation();
            get_editable_content();
            handleTagsInput();
        }
    }
}();