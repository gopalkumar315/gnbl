/**
 * Created by john on 7/18/2015.
 */
AdminNewsList = function () {

    /* DataTable */
    var oTable;
    var initTable = function () {
        var table = $('#news_list_table');
        var baseURL = $('#baseUrl').data('baseurl');
        var categoryId = $('#categoryId').data('value');

        oTable = table.dataTable({
            serverSide: true,
            ajax: {
                url: baseURL + '/admin/news/newslist/' + categoryId,
                type: 'POST'
            },
            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [1,2,5]
                },
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "render": function ( data, type, row ) {
                        return '<a href="' + baseURL + '/admin/news/newsshow/' + row[0] + '">' + row[1].substr(0,70)+'...' + '</a>';
                    },
                    "targets": 1
                },
            ]
        });
        var tableWrapper = $('#news_list_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };

    change_status= function (target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        if(bootbox.confirm('Do You really want to change the status of this news?',function(result) {
                if(result==true){
                    $.post(baseURL+'/admin/news/changenewsstatus',{id:id},function(data) {
                        if(data.status=='success') {
                            oTable._fnAjaxUpdate();
                            toastr['success']("Successfully", "Status Changed");
                        }
                    },'json');
                }
            }));
    };

    newsDelete = function (target) {
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        if(bootbox.confirm('Do You really want to delete this news?',function(result) {
                if(result==true){
                    $.post(baseURL+'/admin/news/newsdelete',{id:id},function(data) {
                        if(data.status=='success') {
                            oTable._fnAjaxUpdate();
                            toastr['success']("Successfully", "News Deleted");
                        }
                    },'json');
                }
            }));
    };

    newsEdit= function (target) {
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        var baseURL = $('#baseUrl').data('baseurl');
        window.location.replace(baseURL + '/admin/news/newsedit/' + id);

    };

    return {
        init: function () {
            initTable();
        }
    };
}();

