/**
 * Created by john on 7/18/2015.
 */
AdminPages = function () {

    /* DataTable */
    var baseURL = $('#baseUrl').data('baseurl');
    var oTable;
    initTable = function () {
        var table = $('#list_table');
        var baseURL = $('#baseUrl').data('baseurl');
        oTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "pageLength": 25,
            "order": [

            ],
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0,1]
                },
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
        });
        var tableWrapper = $('#list_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };
    validate = function (form) {
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);

        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            rules: {
                parent_id: {
                    required: true
                },
                page_label: {
                    required: true
                },
                page_title: {
                    required: true
                },
                page_content: {
                    required: function()
                    {
                        CKEDITOR.instances.page_content.updateElement();
                    }
                }
            },


            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $('.select2me', form).change(function () {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    }

    add_validation=function(){
        var form = $('#add_form');
        $.validator.setDefaults({
            rules: {
                file: {
                    required: true
                },
            },

            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/pages/addpage",
                    data:new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Page Added");
                        CKEDITOR.instances.page_content.setData( '', function() { this.updateElement(); } )
                        $('.select2me', form).select2("val", "");
                        //jQuery('#tags_1').removeTag();
                        form.reset();
                        jQuery('#tags_1').removeTag();
                        location.reload();
                    },
                    error: function () {
                        toastr['error']("error", "Error in adding video")
                    }
                });
                return false;
            }
        });
        validate(form);
    };

    change_status=function(target){
        var id =  target.parentNode.parentNode.childNodes[1].innerHTML;
        bootbox.confirm('Are you sure you want to change the status of this page?', function(result){
            if(result == true) {
                $.post(baseURL+"/admin/pages/changestatus",{id:id},function(data){
                    if(data.status=='success') {
                        toastr['success']('Status Changed');
                        location.reload();
                    } else {
                        toastr['error']('Error In Deletion');
                        oTable._fnAjaxUpdate();
                    }
                },'json');
            }
        });
    };

    delete_page = function(target){
        var id =  target.parentNode.parentNode.childNodes[1].innerHTML;
        bootbox.confirm('Please make sure, All of sub pages of this page will be Deleted!', function(result){
            if(result == true) {
                $.post(baseURL+"/admin/pages/delete",{id:id},function(data){
                    if(data.status=='success') {
                        toastr['success']('Page Deleted');
                        location.reload();
                    } else {
                        toastr['error']('Error In Deletion');
                        oTable._fnAjaxUpdate();
                    }
                },'json');
            }
        });

    };

    edit_page_show=function(target) {
        var id=target.parentNode.parentNode.childNodes[1].innerHTML;
        url = baseURL + "/admin/pages/edit/" + id;
        window.location.replace(url);
    };

    edit_form_content=function() {
        var form = $('#edit_form');
        var id=$('[name="id"]',form).val();
        $.post(baseURL+'/admin/pages/editpage',{id:id},function(data){
            if(data.response_status == 'success'){
                $('[name="page_title"]',form).val(data.page_title);
                $('[name="page_label"]',form).val(data.page_label);
                $('[name="parent_id"]', form).select2("val",data.parent_id);
                $('[name="status"]', form).select2("val",data.status);

                $('[name="tags"]').importTags(data.tags);
                $('#banner_img').attr('src',baseURL+'/web/admin/uploads/'+data.banner);
                CKEDITOR.replace( 'page_content');
                CKEDITOR.instances.page_content.setData(data.page_content);
            }
        },'json');
    };

    edit_validation = function () {
        var form = $('#edit_form');
        $.validator.setDefaults({
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/pages/updatepage",
                    data: new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Page Added");
                    },
                    error: function () {
                        toastr['error']("error", "Error in adding video")
                    }
                });
                return false;
            }
        });
        validate(form);
    };

    readURL=function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banner_img')
                    .attr('src', e.target.result);

            };

            reader.readAsDataURL(input.files[0]);
        }
    };

    var handleTagsInput = function () {
        if (!jQuery().tagsInput) {
            return;
        }
        $('#tags_1').tagsInput({
            width: 'auto',
            'onAddTag': function () {
                //alert(1);
            },
        });
    }

    return {
        init: function () {
            handleTagsInput();

        }
    };
}();

