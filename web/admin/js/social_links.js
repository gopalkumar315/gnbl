/**
 * Created by GOPAL KUMAR on 10/2/2015.
 */
SocialLink = function() {
    var baseURL = $('#baseUrl').data('baseurl');
    var social_link_validation = function () {
        var form = $('#update_form');
        form.submit(function(event){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: baseURL + "/admin/social/update",
                data: $(form).serialize(),
                timeout: 3000,
                success: function () {
                    //Toaster code starts here
                    toastr['success']("Successfully", "Updated");
                    hide_loader();
                    window.location.reload();
                },
                error: function () {
                    toastr['error']("error", "Something went wrong")
                    hide_loader();
                }
            });
            return false;
        })
    };

  return {
      init:function() {
          social_link_validation();
      }
  }
}();