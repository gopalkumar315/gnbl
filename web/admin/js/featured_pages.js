/**
 * Created by john on 10/1/2015.
 */
FeaturedPages = function() {

    var baseURL = $('#baseUrl').data('baseurl');
    edit_form_content=function() {
        var form = $('#edit_form');
        var id=$('[name="id"]',form).val();
        $.post(baseURL+'/admin/featured/edit',{id:id},function(data){
            if(data.response_status == 'success'){
                $('[name="page_title"]',form).val(data.page_title);
                $('[name="page_label"]',form).val(data.page_label);
                $('[name="tags"]').importTags(data.tags);
                $('#banner_img').attr('src',baseURL+'/web/admin/uploads/'+data.banner);
                CKEDITOR.replace( 'page_content');
                CKEDITOR.instances.page_content.setData(data.page_content);
            }
        },'json');
    };


    validate = function (form) {
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            rules: {
                parent_id: {
                    required: true
                },
                page_label: {
                    required: true
                },
                page_title: {
                    required: true
                },
                page_content: {
                    required: function()
                    {
                        CKEDITOR.instances.page_content.updateElement();
                    }
                }
            },


            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        $('.select2me', form).change(function () {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    }

    edit_validation = function () {
        var form = $('#edit_form');
        $.validator.setDefaults({
            submitHandler: function(form) {
                show_loader();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/featured/update",
                    data: new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "Page Updated");
                        hide_loader();
                    },
                    error: function () {
                        toastr['error']("Error", "Something is wrong")
                        hide_loader();
                    }
                });
                return false;
            }
        });
        validate(form);
    };

    readURL=function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banner_img')
                    .attr('src', e.target.result);

            };

            reader.readAsDataURL(input.files[0]);
        }
    };

    var handleTagsInput = function () {
        if (!jQuery().tagsInput) {
            return;
        }
        $('#tags_1').tagsInput({
            width: 'auto',
            'onAddTag': function () {
                //alert(1);
            },
        });
    }

    return {
        init:function() {
            handleTagsInput();
            edit_form_content();
            edit_validation();
        }
    }

}();