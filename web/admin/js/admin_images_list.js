/**
 * Created by john on 7/18/2015.
 */
AdminImagesList = function () {

    /* DataTable */
    var oTable;
    var initTable = function () {

        var table = $('#image_list_table');
        var baseURL = $('#baseUrl').data('baseurl');
        var albumId = $('#albumId').data('value');

        oTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            serverSide: true,
            ajax: {
                url: baseURL + '/admin/images/showimages/' + albumId,
                type: 'POST'
            },
            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [1,2,5]
                },
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "render": function ( data, type, row ) {
                        return '<a class="fancybox" rel="img_group"  href="' + baseURL + '/web/admin/uploads/'+row[2]+'" ><img src="' + baseURL + '/image/web/admin/uploads/'+row[2]+'?s=50x50" title="'+row[1]+'"></a>';
                    },
                    "targets": 2
                }
            ]
        });
        var tableWrapper = $('#image_list_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };

    DropZoneImgaeUpload = function () {

        var baseURL = $('#baseUrl').data('baseurl');
        Dropzone.options.myDropzone = {
            acceptedFiles: ".jpeg,.jpg,.png,.gif",

            init: function () {
                this.on("addedfile", function(file) { });
                this.on("sending", function(file, xhr, formData) {
                    formData.append("title", $('#title').val() ); // Append all the additional input data of your form here!
                });

                this.on('success', function(file,response){
                    if(response.status == 'success'){
                        oTable._fnAjaxUpdate();
                        toastr['success']("Successfully", "Image Successfully Uploaded");

                        var imageId=response.id;
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block' id="+imageId+">Remove file</button>");
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();

                            var id=$(this).attr('id');
                            $.post(baseURL+'/admin/images/remove',{id:id},function(success){
                                oTable._fnAjaxUpdate();
                                toastr['success']("Successfully", "Image Successfully Deleted");
                            },'json');
                            _this.removeFile(file);
                        });
                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    }
                });

                $('#responsive').on('hidden', function (e) {
                    $('#title').val('');
                    Dropzone.forElement("#my-dropzone").removeAllFiles(true);
                })
            }
        }
    };

    imageDelete = function(target) {
        var baseURL = $('#baseUrl').data('baseurl');
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Do you really want to delete this image? ',function(result){
           if(result== true) {
               $.post(baseURL+'/admin/images/remove',{id:id},function(success){
                   oTable._fnAjaxUpdate();
                   toastr['success']("Successfully", "Image Deleted");
               },'json');
           }
        }).off("shown.bs.modal");
    };

    imageEdit= function (target) {
        var baseURL = $('#baseUrl').data('baseurl');
        var id=target.parentNode.parentNode.childNodes[0].innerHTML;
        $.post(baseURL+'/admin/images/edit',{id:id},function(result){
            if(result.status=='success')
            {
                $('#imageUpdate').attr('src', baseURL + '/image/web/admin/uploads/' + result.file+'?s=50x50');
                $('#updateTitle').val(result.title);
                $('#updateId').val(result.id);
                jQuery('#update_image').modal('show');
            }

        },'json');
    }

    imageUpdate = function() {
        var baseURL = $('#baseUrl').data('baseurl');
        var form=$('form#updateForm');
        form.submit(function(e){
            e.preventDefault();
             $.ajax({
                 url: baseURL+'/admin/images/update',
                 type: "POST",             // Type of request to be send, called as method
                 data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                 contentType: false,       // The content type used when sending data to the server.
                 cache: false,             // To unable request pages to be cached
                 processData:false,
                 dataType:'json',
                 success:function(data){
                     if(data.status=='success') {
                         oTable._fnAjaxUpdate();
                         document.getElementById("updateForm").reset();
                         jQuery('#update_image').modal('hide');
                         toastr['success']("Successfully", "Image Updated");
                     } else {
                         toastr['error']("Error", "Something went wrong");
                     }

                 }
            });
            return false;
        });
    }

    preview_image = function () {
        $("#previewImage").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imageUpdate').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    };

    var handleFancybox = function() {
        $(".fancybox").fancybox();
    };

    return {
        init: function () {
            initTable();
            DropZoneImgaeUpload();
            imageUpdate();
            preview_image();
            handleFancybox();
        }
    };
}();

