/**
 * Created by john on 9/26/2015.
 */
download_list = function() {

    /* DataTable */
    var oTable;
    var baseURL = $('#baseUrl').data('baseurl');
    var initTable = function () {
        var table = $('#ajaxDatatable');
        oTable = table.dataTable({
            serverSide: true,
            ajax: {
                url: baseURL + '/admin/download/list/',
                type: 'POST'
            },
            aoColumns: [
                {Data: 'id'},
                {Data: 'title'},
                {Data: 'link'},
                {Data: 'download'},
                {Data: 'status'},
                {Data: 'created'},
            ],
            "order": [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, "All"] // change per page values here
            ],

            "fnDrawCallback":function(){
                copyToClipboard();
            },
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [1,2,5]
                },
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "render": function ( data, type, row ) {
                        return '<button class="btn copy_link btn-xs btn-default" id="copy" data-file="'+baseURL+'/admin/download/file/'+row[0]+'/'+row[3]+'"><i class="fa fa-files-o"></i> Copy Link</button>';
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {
                        return '<button class="btn btn-xs btn-default" onclick="download(this)"><i class="fa fa-cloud-download"></i> Download</button>';
                    },
                    "targets": 3
                },
                {
                    "render": function ( data, type, row ) {
                        if(row[2] == 1){
                            return '<button class="btn btn-xs green" onclick="change_status(this)"></i>Enabled</button>';
                        } else {
                            return '<button class="btn btn-xs red" onclick="change_status(this)"></i>Disabled</button>';
                        }

                    },
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {
                        return row[4];
                    },
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {
                        return '<button class="btn btn-xs btn-default" onclick="delete_file(this)"><i class="fa fa-trash"></i> Delete</button>';
                    },
                    "targets": 6
                },
            ]
        });
        var tableWrapper = $('#ajaxDatatable_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    };

    var add_form_validation = function () {


        var form = $('#add_form');
        var error3 = $('.alert-danger', form);
        var success3 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                title: {
                    required: true,
                },
                file: {
                    required: true,
                },
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success3.hide();
                error3.show();
                //    Metronic.scrollTo(error3, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success3.show();
                error3.hide();
                $.ajax({
                    type: "POST",
                    url: baseURL + "/admin/download/add",
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    timeout: 3000,
                    success: function () {
                        //Toaster code starts here
                        toastr['success']("Successfully", "File Uploaded")
                        //Toaster code ends here
                        jQuery('#add_modal').modal('hide');
                        form.reset();
                        oTable._fnAjaxUpdate();
                    },
                    error: function () {
                        toastr['error']("error", "Something went wrong")
                    }
                });
                return false;
            }

        });

    }

    change_status = function (target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Do you really want to change the status of this File?',function(result) {
            if(result == true) {
                show_loader();
                $.post(baseURL + "/admin/download/changestatus",{id:id},function(response) {
                    if(response.status == 'success') {
                        hide_loader();
                        toastr['success']("Successfully", "Status Changed");
                        oTable._fnAjaxUpdate();
                    } else {
                        hide_loader();
                        toastr['danger']("Error","Something went wrong");
                    }
                },'json');
            }
        })
    };

    delete_file = function (target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Do you really want to Delete this File?',function(result) {
            if(result == true) {
                show_loader();
                $.post(baseURL + "/admin/download/delete",{id:id},function(response) {
                    if(response.status == 'success') {
                        hide_loader();
                        toastr['success']("Successfully", "File Deleted");
                        oTable._fnAjaxUpdate();
                    } else {
                        hide_loader();
                        toastr['danger']("Error","Something went wrong");
                    }
                },'json');
            }
        })
    };

    copyToClipboard=function() {
       $(".copy_link").zclip(
           {
               path: baseURL+'  /web/admin/swf/ZeroClipboard.swf',
               copy: function () {
                   return $(this).data('file');
               },
               afterCopy: function() {
                   $(this).select();
                   toastr['success']("Successfully", "Link Copied");
               }
           });
    };

    download = function(target) {
        var id = target.parentNode.parentNode.childNodes[0].innerHTML;
        window.location.replace(baseURL + "/admin/download/download/"+id);
    };


    return {
        init:function(){
            initTable();
            add_form_validation();
        }
    }
}();