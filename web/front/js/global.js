/**
 * Created by GOPAL KUMAR on 9/22/2015.
 */
global = function() {

    var baseURL = $('#baseUrl').data('baseurl');
    nav_menus=function(){
        $.post(baseURL+'/front/navmenu',{},function(data){
            var nav_menu='';
            $( data ).each(function( index ,value) {
                nav_menu+='<li><a class="dropdown-toggle" href="'+baseURL+'/page/'+value.id+'/'+value.page_label+'">'+value.page_label+'</a>';
                if(value.level.length>0){
                    nav_menu+=' <ul class="dropdown-menu">';
                    $( value.level ).each(function( sub_index ,sub_value) {
                        nav_menu+='<li><a href="'+baseURL+'/page/'+sub_value.id+'/'+sub_value.page_label+'">'+sub_value.page_label+'</a></li>';
                    });
                    nav_menu+='</ul>';
                }
                nav_menu+='</li>';
            });
            $('#nav_menu').append(nav_menu);

        },'json').done(function() {
            nav_menu='<li>' +
                    '<a class="dropdown-toggle"  href="'+baseURL+'/front/contactus">Gallery</a>' +
                    '<ul class="dropdown-menu" >' +
                    '<li><a href="'+baseURL+'/albums"> Photo Gallery </a></li>' +
                    '<li><a href="'+baseURL+'/videoalbum"> Video Gallery </a></li>' +
                    '</ul>' +
                '</li>';
            nav_menu+='<li><a class="dropdown-toggle"  href="'+baseURL+'/front/contactus">Contact Us</a></li>';
            $('#nav_menu').append(nav_menu);

            $('#nav_menu li ').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
            });
        });
    }

    get_news_list = function(target) {
        var title='';
        var news_id='';
        if(target == 0){
            title = 'News';
         news_id = 0;
        } else {
            title = $(target).text();
            news_id = $(target).attr('id');
        }

        //ajax_news_list
        $('.ajax_news_list').text('').stop(true, true).delay(100).fadeOut(300);
        var news_list='';
        var news_category_id='';
        $(target).prop('disabled',true);
        $.post(baseURL+'/front/newslist',{id:news_id},function(data){
            $('.ajax_news_list').text('').stop(true, true).delay(100).fadeOut(300);

            $('.ajax_news_heading').text(title );
            $( data ).each(function( index ,value) {
                news_list+='<a href="'+baseURL+'/shownews/'+value.id+"/"+value.title+'">'+value.title.substring(0,100)+' <h6 style="display:inline-block">('+value.date_time+')</h6><a>';
                news_category_id = value.news_category_id;
            });

            if(data.length > 0){
                news_list+='<div><a class="text-right" href="'+baseURL+'/front/allnews/'+news_category_id+'">more news</a></div>';
            } else {
                news_list+='<div><a class="text-left" href="javascript:;">No any news</a></div>';
            }

            $('.ajax_news_list').append(news_list).fadeIn();
        },'json');
    };

    get_footer_menus = function(){
        $.post(baseURL+'/front/footermenus',{},function(data){
            var nav_menu='';
            $( data.menuBar ).each(function( index ,value) {
                nav_menu+='<li><a  href="'+baseURL+'/page/'+value.id+'/'+value.page_label+'">'+value.page_label+'</a>';
                nav_menu+='</li>';
            });
            $('#footerMenus').append(nav_menu);


            var news_list='';
            $( data.newsList ).each(function( index ,value) {
                news_list+='<li><a  href="'+baseURL+'/front/allnews/'+value.id+'">'+value.name+'</a>';
                news_list+='</li>';
            });
            $('#footerNews').append(news_list);

            var social_links ='';
            $( data.socialLinks ).each(function( index ,value) {
                social_links+='<li><a  href="'+value.link+'" target="_blank"><i class="fa fa-lg '+value.icon+'"></i></a>';
                social_links+='</li>';
            });
            $('#social_links').append(social_links);



        });
    }





    return {
        init:function(){
            nav_menus();
            get_footer_menus();
            get_footer_news_list();
        }
    }
}();